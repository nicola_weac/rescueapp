<?php

return [
    'model' => \App\Models\Media::class,
    'public_path' => public_path(),
    'files_directory' => 'uploads/',
    'files_tmp_directory' => 'uploads/tmp/',
    'sub_directories' => true,
    'rename' => 'nothing'

];