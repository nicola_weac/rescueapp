<?php

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \Illuminate\Support\Facades\Route;

Route::middleware('backend')->namespace('\Auth')->group(function (){

    Route::get('/', 'LoginController@showLoginForm')->name('admin.login');
    Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'LoginController@login')->name('admin.login');
    Route::post('/logout', 'LoginController@logout')->name('admin.logout');

});

Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
Route::get('dashboard/new', 'DashboardController@newele')->name('admin.dashboard.new');

Route::prefix('user')->group(function (){
    Route::get('', 'UserController@index')->name('admin.user.index');
    Route::get('{userid}/profile', 'UserController@profile')->name('admin.user.profile');
    Route::post('{userid}/profile', 'UserController@profileSave')->name('admin.user.profile.save');
    Route::get('{userid}/password', 'UserController@password')->name('admin.user.password');
    Route::post('{userid}/password', 'UserController@passwordSave')->name('admin.user.password.save');
    Route::get('{userid}/trackings', 'UserController@trackings')->name('admin.user.trackings');
    Route::get('{userid}/contacts', 'UserController@contacts')->name('admin.user.contacts');

    Route::get('{userid}/delete', 'UserController@delete')->name('admin.user.delete');
});

Route::prefix('member')->group(function (){
    Route::get('', 'MembersController@index')->name('admin.member.index');
    Route::get('{memberid}/profile', 'MembersController@profile')->name('admin.member.profile');
    Route::post('{memberid}/profile', 'MembersController@profileSave')->name('admin.member.profile.save');
});

Route::prefix('content')->group(function (){
    Route::get('content', 'ContentController@showAll')->name('admin.content');
    Route::get('content/add', 'ContentController@add')->name('admin.content.add');
    Route::post('content/add', 'ContentController@addSave')->name('admin.content.add.save');
    Route::get('content/{contentid}/edit', 'ContentController@edit')->name('admin.content.edit');
    Route::post('content/{contentid}/edit', 'ContentController@editSave')->name('admin.content.edit.save');
});

Route::prefix('tracking')->group(function (){
    Route::get('', 'TrackingController@showAll')->name('admin.tracking');
    Route::get('{trackingid}/info', 'TrackingController@showInfo')->name('admin.tracking.info');
    Route::get('{trackingid}/info/end', 'TrackingController@ende')->name('admin.tracking.end');
    Route::get('{trackingid}/info/route', 'TrackingController@showRoute')->name('admin.tracking.route');

    Route::get('{trackingid}/info/media/', 'TrackingController@showMedia')->name('admin.tracking.info.media');
    Route::post('{trackingid}/info/ajaxmedia/', 'TrackingController@showMediaAjax')->name('admin.tracking.info.mediaajax');
    Route::get('media/show/{path}', 'TrackingController@loadMedia')->name('admin.load.media');

});

Route::prefix('license')->group(function (){
    Route::get('list', 'LicenseController@list')->name('admin.license.list');
    Route::get('add', 'LicenseController@add')->name('admin.license.add');
    Route::post('add', 'LicenseController@addSave')->name('admin.license.add.save');
});

Route::delete('delete/trackings/{trackingid}', 'ControlController@deleteTrackings')->name('api.delete.trackings');
Route::delete('delete/emergencies/{emergencyid}', 'ControlController@deleteEmergencies')->name('api.delete.emergencies');
Route::delete('delete/user/{userid}', 'ControlController@deleteUsers')->name('api.delete.user');


// Notifications
Route::post('notifications', 'NotificationController@store');
Route::get('notifications', 'NotificationController@index');
Route::patch('notifications/{id}/read', 'NotificationController@markAsRead');
Route::post('notifications/mark-all-read', 'NotificationController@markAllRead');
Route::post('notifications/{id}/dismiss', 'NotificationController@dismiss');
// Push Subscriptions
Route::post('subscriptions', 'PushSubscriptionController@update');
Route::post('subscriptions/delete', 'PushSubscriptionController@destroy');
// Manifest file (optional if VAPID is used)
Route::get('manifest.json', function () {
    return [
        'name' => config('app.name'),
        'gcm_sender_id' => config('webpush.gcm.sender_id')
    ];
});