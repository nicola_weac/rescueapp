<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|---------------------------------------------------------------£-----------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/auth')->namespace('Auth')->group(function (){
    Route::post('/login/social', 'SocialAuthController@createOrGetUser')->name('admin.social.login');
    Route::post('login', 'AuthController@login')->name('api.login');
    Route::post('register', 'AuthController@register')->name('api.register');

    Route::post('check/username', 'AuthHelpController@usernameCheck')->name('api.check.username');
    Route::post('check/email', 'AuthHelpController@emailCheck')->name('api.check.email');

    Route::post('reset/email', 'ForgotPasswordController@sendResetCode')->name('api.password.email');
    Route::post('reset/password', 'ForgotPasswordController@reset')->name('api.password.reset');
});

#Route::namespace('\Api')->group(function (){
    Route::post('tracking/start', 'TrackingController@startTracking')->name('api.tracking.start');
    Route::post('tracking/stop', 'TrackingController@stopTracking')->name('api.tracking.stop');
    Route::get('tracking/history', 'TrackingController@showHistory')->name('api.tracking.history');
    Route::post('update/position', 'PositionController@updatePosition')->name('api.position.update');
    Route::post('tracking/countdown', 'TrackingController@updateCountdown')->name('api.tracking.countdown');
    Route::post('tracking/{trackingid}/cancel', 'TrackingController@cancelTracking')->name('api.tracking.cancel');
    Route::get('tracking/current', 'TrackingController@getCurrentTracking')->name('api.tracking.current');

    Route::post('tracking/{trackingid}/accept', 'TrackingController@accept')->name('api.tracking.accept');
    Route::post('tracking/{trackingid}/deny', 'TrackingController@deny')->name('api.tracking.deny');

    Route::get('user/position', 'PositionController@showPositions')->name('api.user.positions');

    Route::get('contacts', 'ContactController@showContacts')->name('api.contact');
    Route::post('contacts/add', 'ContactController@addContact')->name('api.contact.add');
    Route::get('contacts/{contactid}/delete', 'ContactController@deleteContact')->name('api.contact.delete');
    Route::post('contacts/{contactid}/default', 'ContactController@defaultContact')->name('api.contact.default');

    Route::post('alive', 'TrackingController@alive')->name('api.client.alive');

    Route::post('emergency', 'EmergencyController@sendEmergency')->name('api.emergency');
    Route::post('emergency/files', 'EmergencyFileController@sendEmergencyFiles')->name('api.emergency.files');

    Route::get('user', 'UserController@showProfile')->name('api.user');

    Route::post('user/media/update', 'UserController@updateMedia')->name('api.user.media.update');
    Route::post('user/update', 'UserController@updateProfile')->name('api.user.update');
    Route::post('/user/password', 'UserController@passwordEdit')->name('api.user.password.edit');
    Route::delete('user/delete/account', 'UserController@deleteAccount')->name('api.user.delete.account');

    Route::post('password/edit', 'PasswordController@editPassword')->name('api.password.edit');

    Route::get('content/{identifier}', 'ContentController@index')->name('api.content');

    Route::get('delphos/license', 'DelphosLicenseController@hasLicense')->name('api.delphos.license');
    Route::post('delphos/license/info', 'DelphosLicenseController@sendInfoEmail')->name('api.license.info');

    Route::get('pushnotifications/{userid}', 'PushNotificationsController@show')->name('api.push.show');
    Route::post('pushnotifications', 'PushNotificationsController@createOrUpdate')->name('api.push.create/update');
    Route::post('pushnotifications/logout', 'PushNotificationsController@delete')->name('api.push.delete');

    Route::post('/repeat_notifications', 'UserController@repeat_notifications')->name('api.push.repeat');

    Route::get('/emergency/todo', 'EmergencyToDoController@showToDo')->name('api.emergency.todo');
    Route::post('/emergency/todo', 'EmergencyToDoController@update')->name('api.emergency.todo.update');

    Route::get('start/notifications', 'NotificationController@showNotificationsOnStart')->name('api.notifications.start');

#});