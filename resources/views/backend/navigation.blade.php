<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">HAUPTNAVIGATION</li>
            <li class="">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="">
                <a href="{{ route('admin.user.index') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Nutzer</span>
                </a>
            </li>
            <li class="">
                <a href="{{ route('admin.tracking') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Trackings</span>
                </a>
            </li>
            <li class="">
                <a href="{{ route('admin.member.index') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Administratoren</span>
                </a>
            </li>
            <li class="">
                <a href="{{ route('admin.content') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Inhalte</span>
                </a>
            </li>
            <li class="">
                <a href="{{ route('admin.license.list') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Lizenzen</span>
                </a>
            </li>

            {{--<li class="header">LABELS</li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>