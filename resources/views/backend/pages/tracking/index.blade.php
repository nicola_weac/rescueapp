@extends('backend.layouts.layout')

@section('title', 'Trackings')

@section('content-header')
<section class="content-header">
    <h1>
        Trackings
        <small>Alle</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="active"><a href="{{ route('admin.tracking') }}"><i class="fa fa-dashboard"></i> Alle Trackings</a></li>
    </ol>
</section>
@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <i class="fa fa-dashboard">&nbsp;</i>
                        Alle Trackings
                    </h3>
                </div>
                <div class="box-body">
                    <fieldset>
                        <legend>Alle Trackings</legend>

                        @if($Trackings->count() > 0)
                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>Tracking-ID</th>
                                <th>User</th>
                                <th>Benachrichtigungsperiode</th>
                                <th>Notfall gerufen</th>
                                <th>Delphos Vertrag?</th>
                                <th>Status</th>
                                <th>Aktionen</th>
                            </tr>
                            @foreach($Trackings as $key => $value)
                                <tr>
                                    <td>{{ $value->id }}</td>
                                    <td>{{ $value->userprofile->firstname . ' ' . $value->userprofile->lastname }}</td>
                                    <td>{{ $value->notify_period }} Minuten</td>
                                    @if($value->emergency == 1)
                                    <td>Ja</td>
                                    @else
                                    <td>Nein</td>
                                    @endif

                                    <td>@if($value->user->delphosLicense->count() > 0) Ja @else Nein @endif</td>

                                    @if($value->status == 1)
                                    <td>Beendet</td>
                                    @else
                                    <td>Aktiv</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.tracking.info', $value->id)  }}" class="btn btn-block btn-info btn-sm">
                                            <i class="fa fa-info">&nbsp;</i>
                                            Info
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        @else
                            <p>Keine Daten gefunden.</p>
                        @endif

                    </fieldset>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        {{ $Trackings->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection