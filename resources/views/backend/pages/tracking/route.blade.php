@extends('backend.layouts.layout')

@section('title', 'Tracking Info')

@section('content-header')
    <section class="content-header">
        <h1>
            Tracking
            <small>{{ $Tracking->userprofile->firstname . ' ' . $Tracking->userprofile->lastname }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active"><a href="{{ route('admin.tracking.info', $Tracking->id) }}"><i class="fa fa-dashboard"></i> Tracking Info</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form role="form" class="form-horizontal" method="POST" action="">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <i class="fa fa-dashboard">&nbsp;</i>
                                Tracking ID: {{ $Tracking->id }}
                            </h3>
                        </div>
                        <div class="box-body">
                            <div class="col-sm-12" style="border-bottom: 1px solid #4b646f; padding-bottom: 20px; margin-bottom: 30px;">
                                <div class="col-sm-2 col-sm-offset-3" style="border-right: 1px solid crimson;">
                                    <img src="{{ $profilePicture }}" alt="ProfilBild">
                                </div>
                                <div class="col-sm-2" style="border-right: 1px solid crimson;">
                                    <p style="font-size: 26px; font-weight: bolder;">{{ $Tracking->userprofile->firstname . ' ' . $Tracking->userprofile->lastname }}</p>
                                    <p>{{ $Tracking->userprofile->phone }}</p>
                                    <p>{{ $Tracking->userprofile->street . ', ' . $Tracking->userprofile->zip . ' ' . $Tracking->userprofile->city }}</p>
                                </div>
                                <div class="col-sm-2">
                                    <p style="font-size: 26px; font-weight: bolder;">Tracking ID: {{ $Tracking->id }}</p>
                                    <p>{{ $Tracking->created_at }}</p>
                                </div>

                            </div>

                            <div class="col-sm-12" style="margin-bottom: 30px;">
                                <div id="map"></div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <div class="pull-right">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('template_linked_css')
    <style>
        #map {
            height: 700px;
            width: 100%;
        }
    </style>
@endsection
@section('template_linked_js')
    <script>
        function initMap() {
            var start = {lat: {{ $route->from_lat }}, lng: {{ $route->from_lng }}};
            var end = {lat: {{ $route->to_lat }}, lng: {{ $route->to_lng }}};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 7,
                center: start
            });
            var marker = new google.maps.Marker({
                position: start,
                map: map
            });
            var marker2 = new google.maps.Marker({
                position: end,
                map: map
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKA1xP97auygGzcFXg0Ql6dK64Cf07WO4&callback=initMap">
    </script>
@endsection