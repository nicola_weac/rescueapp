@extends('backend.layouts.layout')

@section('title', 'Tracking Info')

@section('content-header')
    <section class="content-header">
        <h1>
            Tracking
            <small>{{ $Tracking->userprofile->firstname . ' ' . $Tracking->userprofile->lastname }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active"><a href="{{ route('admin.tracking.info', $Tracking->id) }}"><i class="fa fa-dashboard"></i> Tracking Info</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form role="form" class="form-horizontal" method="POST" action="">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <i class="fa fa-dashboard">&nbsp;</i>
                                Tracking ID: {{ $Tracking->id }} ({{ $Tracking->created_at }})
                            </h3>
                            <div class="pull-right margin-r-5">
                                @if($Tracking->battery()->count() > 0)
                                    @if($Tracking->battery()->get()->last()->battery_state == 'charging' OR $Tracking->battery()->get()->last()->battery_state == 'full')
                                        <div class="flex-center">
                                            <img class="margin-r-5" height="18px" src="{{ asset('Battery_Charging.png') }}" alt="CHARGING">
                                            <span>{{ $Tracking->battery()->get()->last()->battery_lvl }}%</span>
                                        </div>
                                    @else
                                        @if($Tracking->battery()->get()->last()->battery_lvl <= 20)
                                            <div class="flex-center">
                                                <img class="margin-r-5" height="18px" src="{{ asset('Battery_20.png') }}" alt="LOW">
                                                <span>{{ $Tracking->battery()->get()->last()->battery_lvl }}%</span>
                                            </div>
                                        @elseif($Tracking->battery()->get()->last()->battery_lvl <= 80)
                                            <div class="flex-center">
                                                <img class="margin-r-5" height="18px" src="{{ asset('Battery_20_100.png') }}" alt="MEDIUM">
                                                <span>{{ $Tracking->battery()->get()->last()->battery_lvl }}%</span>
                                            </div>
                                        @endif
                                        @if($Tracking->battery()->get()->last()->battery_lvl > 80)
                                            <div class="flex-center">
                                                <img class="margin-r-5" height="18px" src="{{ asset('Battery_Full.png') }}" alt="FULL">
                                                <span>{{ $Tracking->battery()->get()->last()->battery_lvl }}%</span>
                                            </div>
                                        @endif
                                    @endif
                                @endif
                            </div>
                            @if($Tracking->route()->count() > 0)
                                <div class="pull-right margin-r-5">
                                    <a href="{{ route('admin.tracking.route', $Tracking->id) }}" class="btn btn-xs btn-info">
                                        <i class="fa fa-medium">&nbsp;</i>
                                        Route anzeigen
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="box-body">
                            <div class="col-sm-12" style="border-bottom: 1px solid #4b646f; padding-bottom: 20px; margin-bottom: 30px;">
                                @if($Tracking->emergency == 1 && $Tracking->status == 0)
                                    <div class="col-sm-12 text-right">
                                        <a href="{{ route('admin.tracking.end', $Tracking->id) }}" class="btn btn-lg btn-danger">Notfall beenden</a>
                                    </div>
                                @endif
                                <div class="col-sm-2">
                                    <img src="{{ $profilePicture->url or '/img/default-user-img.jpg' }}" alt="ProfilBild" style="width: 100%;">
                                </div>
                                <div class="col-sm-3">
                                    <p  style="font-size: 26px; font-weight: bolder;">
                                        Nutzer:
                                    </p>
                                    <p>
                                        {{ $Tracking->userprofile->firstname}}
                                        {{ $Tracking->userprofile->lastname}} (@if($Tracking->userprofile->height != null) {{$Tracking->userprofile->height}} cm Groß @endif @if($Tracking->userprofile->age != null) {{$Tracking->userprofile->age}} Jahre @endif , @if($Tracking->userprofile->gender == "male") männlich @else weiblich  @endif )
                                    </p>
                                    <p>
                                        Telefon: {{ $Tracking->userprofile->phone or "" }}</p>
                                    <p>
                                        Adresse: {{ $Tracking->userprofile->street . ', ' . $Tracking->userprofile->zip . ' ' . $Tracking->userprofile->city }}</p>
                                </div>
                                <div class="col-sm-3">
                                    <p  style="font-size: 26px; font-weight: bolder;">
                                        Weitere Informationen:
                                    </p>
                                    Unverträglichkeiten: {{$Tracking->userprofile->intolerances}}<br>
                                    Vorerkrankungen: {{$Tracking->userprofile->pre_existing_conditions}}<br>
                                    Medikation: {{$Tracking->userprofile->medication}}<br>
                                    Blutgruppe: {{$Tracking->userprofile->blood_type}}<br>
                                </div>
                                {{--
                                <div class="col-sm-3">
                                    <p  style="font-size: 26px; font-weight: bolder;">
                                        Medien:
                                    </p>
                                    <ul class="medias">
                                        @if($Tracking->emergency()->count() > 0)
                                            @if($Tracking->emergency()->first()->files()->count() > 0)
                                                @foreach($Tracking->emergency()->first()->files()->orderByDesc("id")->limit(5)->get() as $file)
                                                    <li>
                                                        <a href="javascript:void(0);" data-video="/admin/tracking/media/show/{{$file->id}}" class="file">
                                                            {{$file->created_at->format("d.m H:i:s")}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            @endif
                                        @endif
                                    </ul>
                                </div>--}}
                            </div>


                                <div class="col-sm-12">
                                    <div class="col-sm-6" style="margin-bottom: 30px;">
                                        <div id="map"></div>
                                    </div>
                                    <div class="col-sm-6 player" style="margin-bottom: 30px;">
                                        @if($Tracking->emergency == 1 && $Tracking->status == 0)
                                            <a href="javascript:void(0);" class="btn btn-success" style="width:300px;font-size:30px;margin:auto;" >Stream starten</a>
                                            <div class="iframe"></div>
                                        @endif
                                    </div>
                                </div>

                        </div>
                        <div class="box-footer">

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('template_linked_css')
    <style>
        #map {
            height: 700px;
            width: 100%;
        }

        .flex-center{
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
@endsection
@section('template_linked_js')


<script>
    $(".player").click(function(){
        $(".player a").hide();
        $(".iframe").html("<iframe src='/stream.php?user=37' style='width:420px; height: 320px; border: 0px;'></iframe>");
    });

    var marker = "";
    @if(!empty($position->lat))
    function initMap() {
        var uluru = {lat: {{ $position->lat or null }}, lng: {{ $position->lng or null }}};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: uluru
        });
        marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
    @else
    function initMap() {
        var uluru = {lat: {{ $Tracking->emergency()->first()->lat or null }}, lng: {{ $Tracking->emergency()->first()->lng or null }}};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: uluru
        });
        marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
    @endif

    $( document ).ready(function() {


        window.setInterval(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            $.ajax({
                method: "POST",
                url: "/admin/tracking/{{ $Tracking->id }}/info/ajaxmedia",
            }).done(function( data ) {
                $(".medias").html(data.message);
                var lat = parseFloat(data.lat);
                var lng = parseFloat(data.lng);
                var newLatLng = new google.maps.LatLng(lat, lng);
                marker.setPosition(newLatLng);
            }).fail(function( ) {

            });
        }, 5000);

        $(document).on('click','.file',function(){
             var data = $(this).data("video");
             $(".player").html('<video  style="height: 500px;width: 100%;" controls autoplay> <source src="'+data+'"> </video>');
         });

    });
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKA1xP97auygGzcFXg0Ql6dK64Cf07WO4&callback=initMap">
</script>
@endsection