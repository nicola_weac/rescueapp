@extends('backend.layouts.layout')

@section('title', 'Dashboard')

@section('content-header')
    <section class="content-header">
        <h1>
            Übersicht
            <small>Übersicht aller Trackings</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            {{--<div class="col-md-6">
               <h4>Filter</h4>
                <p>
                    <select name="" class="changer">
                        <option value="">Alle</option>
                        <option value="bg-green-gradient">Normal</option>
                        <option value="bg-yellow-gradient">externe Gefahr</option>
                        <option value="bg-red-gradient">Delphos Gefahr</option>
                    </select>
                </p>
            </div> --}}
        </div>
        <div class="row inserts">

            <div class="col-md-8 ok">
                <h2>Überwachung</h2>
                <div class="inner">
                    @if($Tracking->count() > 0)

                            @foreach($Tracking as $key => $value)
                                <div class="col-lg-4 col-xs-12">
                                    <!-- small box -->
                                    @if($value->emergency == null)
                                        <div class="small-box bg-green-gradient">

                                            <div class="inner">
                                                @if($value->route()->count() > 0)
                                                    {{ $value->id }}
                                                    <p style="font-size: 26px;"><b>Route von</b> {{ $value->userprofile->firstname}} {{ $value->userprofile->lastname}}</p>
                                                    <p>Von: {{ $value->route != null ? $value->route->from_city : '---' }}</p>
                                                    <p>Bis: {{ $value->route != null ? $value->route->to_city : '---' }}</p>
                                                @else
                                                    <p style="font-size: 26px;"><b>Überwachung von</b> {{ $value->userprofile->firstname}} {{ $value->userprofile->lastname}}</p>
                                                    <p>Von: --- </p>
                                                    <p>Bis: ---</p>
                                                @endif
                                            </div>
                                            <div class="icon">
                                                <i class="fa"></i>
                                            </div>
                                            <a href="{{ route('admin.tracking.info', $value->id) }}" class="small-box-footer">
                                                Mehr Informationen <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        </div>
                                    @else
                                        @if($value->delphos == 0)
                                            <div class="small-box bg-yellow-gradient">
                                                <a href="{{ route('admin.tracking.end', $value->id) }}" style="position: absolute;color:#fff;right:0px;margin-right: 10px;margin-top: 10px; font-weight:bold;">beenden</a>
                                                <div class="inner">
                                                    @if($value->route()->count() > 0)
                                                        <p style="font-size: 26px;">
                                                            <b>Route von:</b> <br>
                                                            {{ $value->userprofile->firstname}} {{ $value->userprofile->lastname}}
                                                        </p>
                                                    @else
                                                        <p style="font-size: 26px;">
                                                            <b>Überwachung:</b> <br>
                                                            {{ $value->userprofile->firstname}} {{ $value->userprofile->lastname}}</p>
                                                    @endif
                                                    <p>Von: {{ $value->route != null ? $value->route->from_city : '---' }}</p>
                                                    <p>Bis: {{ $value->route != null ? $value->route->to_city : '---' }}</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-"></i>
                                                </div>
                                                <a href="{{ route('admin.tracking.info', $value->id) }}" class="small-box-footer">
                                                    Alarm annehmen <i class="fa fa-arrow-circle-right"></i>
                                                </a>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            @endforeach

                    @endif
                </div>
            </div>

            <div class="col-md-4 errors">
                <h2>Notfälle</h2>
                <div class="inner">
                    @if($Tracking->count() > 0)

                            @foreach($Tracking as $key => $value)
                                <div class="col-lg-12 col-xs-12">
                                    <!-- small box -->
                                    @if($value->emergency != null)
                                        @if($value->delphos == 1)
                                            <div class="small-box  bg-red-gradient">
                                                <a href="{{ route('admin.tracking.end', $value->id) }}" style="position: absolute;color:#fff;right:0px;margin-right: 10px;margin-top: 10px; font-weight:bold;">beenden</a>
                                                <div class="inner">
                                                    @if($value->route()->count() > 0)
                                                        <p style="font-size: 26px;">
                                                            <b>Route:</b> <br>
                                                            {{ $value->userprofile->firstname}} {{ $value->userprofile->lastname}}
                                                        </p>
                                                    @else
                                                        <p style="font-size: 26px;">
                                                            <b>Überwachung:</b> <br>
                                                            {{ $value->userprofile->firstname}} {{ $value->userprofile->lastname}}</p>
                                                    @endif
                                                    <p>Von: {{ $value->route != null ? $value->route->from_city : '---' }}</p>
                                                    <p>Bis: {{ $value->route != null ? $value->route->to_city : '---' }}</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-"></i>
                                                </div>
                                                <a href="{{ route('admin.tracking.info', $value->id) }}" class="small-box-footer">
                                                    Alarm annehmen <i class="fa fa-arrow-circle-right"></i>
                                                </a>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            @endforeach

                    @endif
                </div>
            </div>
        </div>
    </section>

    <notifications-demo></notifications-demo>

@endsection

@section('template_linked_js')

@endsection