<?php
/**
 * Created by PhpStorm.
 * User: bendixnicola
 * Date: 20.03.18
 * Time: 12:47
 */
?>
@extends('backend.layouts.layout')

@section('title', 'Inhalte')

@section('content-header')
    <section class="content-header">
        <h1>
            Inhalte
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active"><a href=""><i class="fa fa-dashboard"></i> Inhalt Liste</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            <i class="fa fa-dashboard">&nbsp;</i>
                            Inhalt Liste
                        </h3>
                        <div class="pull-right">
                            <a href="{{ route('admin.content.add') }}" class="btn btn-info btn-xs">
                                <i class="fa fa-plus-circle">&nbsp;</i>
                                Inhalt hinzufügen
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <fieldset>
                            <legend>Alle Inhalte</legend>

                            @if($Contents->count() > 0)
                                <table class="table table-bordered table-responsive">
                                    <tr>
                                        <th>Identifier</th>
                                        <th>Name</th>
                                        <th>Aktionen</th>
                                    </tr>
                                    @foreach($Contents as $key => $value)
                                        <tr>
                                            <td>{{ $value->identifier }}</td>
                                            <td>{{ substr(strip_tags($value->text),0,200) }} ...</td>
                                            <td>
                                                <a href="{{ route('admin.content.edit', $value->id) }}" class="btn btn-warning btn-sm btn-block">
                                                    <i class="fa fa-pencil">&nbsp;</i>
                                                    Bearbeiten
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            @else
                                <p>Keine Daten gefunden.</p>
                            @endif

                        </fieldset>
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection