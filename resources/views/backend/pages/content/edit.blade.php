<?php
/**
 * Created by PhpStorm.
 * User: bendixnicola
 * Date: 20.03.18
 * Time: 12:47
 */
?>
@extends('backend.layouts.layout')

@section('title', 'Inhalt')

@section('content-header')
    <section class="content-header">
        <h1>
            Inhalt
            <small>Bearbeiten</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active"><a href=""><i class="fa fa-dashboard"></i> Inhalt bearbeiten</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.content.edit.save', $Content->id) }}" role="form" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                <i class="fa fa-dashboard">&nbsp;</i>
                                Inhalt bearbeiten
                            </h3>
                        </div>
                        <div class="box-body">
                            <fieldset>
                                <legend>Inhalt: {{ $Content->name }}</legend>

                                <div class="form-group{{ $errors->has('identifier') ? ' has-error ' : '' }}">
                                    <label for="identifier" class="control-label col-sm-2">Identifier</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="identifier" id="identifier" placeholder="Identifier eingeben" value="{{ old('identifier', $Content->identifier) }}" class="form-control">
                                        @if($errors->has('identifier'))
                                            <span class="help-block"><strong>{{ $errors->first('identifier') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('text') ? ' has-error ' : '' }}">
                                    <label for="text" class="control-label col-sm-2">Text</label>
                                    <div class="col-sm-8">
                                        <textarea name="text" id="text" cols="30" rows="10" class="form-control">
                                            {{ $Content->text }}
                                        </textarea>
                                        @if($errors->has('text'))
                                            <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="col-xs-6 col-xs-offset-3">
                                <button type="submit" class="btn btn-block btn-success">
                                    <i class="fa fa-save">&nbsp;</i>
                                    Speichern
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection