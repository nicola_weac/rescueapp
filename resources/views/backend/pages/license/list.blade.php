<?php
/**
 * Created by PhpStorm.
 * User: bendixnicola
 * Date: 20.03.18
 * Time: 12:47
 */
?>
@extends('backend.layouts.layout')

@section('title', 'Lizenzen')

@section('content-header')
    <section class="content-header">
        <h1>
            Delphos
            <small>Lizenzen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active"><a href=""><i class="fa fa-dashboard"></i> Delphos Lizenzen</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            <i class="fa fa-dashboard">&nbsp;</i>
                            Delphos Lizenzen
                        </h3>
                        <div class="pull-right">
                            <a href="{{ route('admin.license.add') }}" class="btn btn-info btn-xs">
                                <i class="fa fa-plus-circle">&nbsp;</i>
                                Lizenz hinzufügen
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <fieldset>
                            <legend>Alle Lizenzen</legend>

                            @if($Licenses->count() > 0)
                                <table class="table table-bordered table-responsive">
                                    <tr>
                                        <th>Email des Users</th>
                                        <th>Anfangsdatum</th>
                                        <th>Auslaufdatum</th>
                                    </tr>
                                    @foreach($Licenses as $key => $value)
                                        <tr>
                                            <td>{{ $value->user->email }}</td>
                                            <td>{{ substr($value->updated_at, 0, 10) }}</td>
                                            <td>{{ $value->expiry_date }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            @else
                                <p>Keine Daten gefunden.</p>
                            @endif

                        </fieldset>
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection