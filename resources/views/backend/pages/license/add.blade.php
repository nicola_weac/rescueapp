<?php
/**
 * Created by PhpStorm.
 * User: bendixnicola
 * Date: 20.03.18
 * Time: 12:47
 */
?>
@extends('backend.layouts.layout')

@section('template_linked_css')
    <link rel="stylesheet" href="/public/plugins/datepicker/datepicker3.css">
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('title', 'Lizenz hinzufügen')

@section('content-header')
    <section class="content-header">
        <h1>
            Lizenz
            <small>hinzufügen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active"><a href=""><i class="fa fa-dashboard"></i> Lizenz hinzufügen</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.license.add.save') }}" role="form" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                <i class="fa fa-dashboard">&nbsp;</i>
                                Lizenz hinzufügen
                            </h3>
                        </div>
                        <div class="box-body">
                            <fieldset>
                                <legend>Lizenz hinzufügen</legend>

                                <div class="form-group{{ $errors->has('user_id') ? ' has-error ' : '' }}">
                                    <label for="user_id" class="control-label col-sm-2">User auswählen</label>
                                    <div class="col-sm-8">
                                        <select name="user_id" id="user_id" class="form-control select2">
                                            @foreach($Users as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->email }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('user_id'))
                                            <span class="help-block"><strong>{{ $errors->first('user_id') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('expiry_date') ? ' has-error ' : '' }}">
                                    <label for="expiry_date" class="control-label col-sm-2">Text</label>
                                    <div class="col-sm-8">
                                        <input type="date" id="expiry_date" class="datepicker form-control" name="expiry_date">
                                        @if($errors->has('expiry_date'))
                                            <span class="help-block"><strong>{{ $errors->first('expiry_date') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="box-footer clearfix">
                            <div class="col-xs-6 col-xs-offset-3">
                                <button type="submit" class="btn btn-block btn-success">
                                    <i class="fa fa-save">&nbsp;</i>
                                    Speichern
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('template_linked_js')
    <script src="/public/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/public/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $(".select2").select2();
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd'
            });
        });
    </script>
@endsection