@extends('backend.layouts.layout')

@section('title', 'Member Index')

@section('content-header')
<section class="content-header">
    <h1>
        Member
        <small>Index</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="active"><a href="{{ route('admin.member.index') }}"><i class="fa fa-dashboard"></i> Member Index</a></li>
    </ol>
</section>
@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <i class="fa fa-dashboard">&nbsp;</i>
                        Member Index
                    </h3>
                </div>
                <div class="box-body">
                    <fieldset>
                        <legend>Alle Member</legend>

                        @if($Members->count() > 0)
                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Registriert am:</th>
                                <th>Aktionen</th>
                            </tr>
                            @foreach($Members as $key => $value)
                                <tr>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>
                                        <a href="{{ route('admin.member.profile', $value->id) }}" class="btn btn-sm btn-info btn-block">
                                            <i class="fa fa-pencil">&nbsp;</i>
                                            Mehr Informationen
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        @else
                            <p>Keine Daten gefunden.</p>
                        @endif

                    </fieldset>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        {{ $Members->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection