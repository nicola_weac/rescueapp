@extends('backend.layouts.layout')

@section('title', 'Passwort ändern')

@section('template_linked_css')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content-header')
    <section class="content-header">
        <h1>
            {{ $User->name }}
            <small>Passwort ändern</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ route('admin.user.index') }}"><i class="fa fa-dashboard"></i> User Index</a></li>
            <li class="active">User bearbeiten</li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">

        <div class="row">
            <div class="col-md-3">
                <!-- User Profile Image -->
                <div class="box">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive" src="{{ $User->media()->get()->last()->url or '' }}" alt="User profile picture">
                        <h3 class="profile-username text-center">{{ $User->name }}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Navigation</h3>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav  nav-stacked">
                            <li><a href="{{ route('admin.user.profile', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Profil</a></li>
                            <li class="active"><a href="{{ route('admin.user.password', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Passwort ändern</a></li>
                            <li><a href="{{ route('admin.user.trackings', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Trackings</a></li>
                            <a href="{{ route('admin.user.delete', $User->id) }}" onclick="return confirm('Wirklich löschen?');" class="btn btn-block btn-danger"><i class="fa fa-trash"></i> User löschen</a>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>

            <div class="col-md-9">

                <form role="form" class="form-horizontal" method="POST" action="{{ route('admin.user.password.save', $User->id) }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="box">
                        <div class="box-header with-border">
                            <i class="fa fa-warning"></i>
                            <h3 class="box-title">{{ $User->name }}'s Passwort ändern</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <fieldset>
                                <legend>Passwort ändern</legend>

                                <div class="form-group{{ $errors->has('password') ? ' has-error ' : '' }}">
                                    <label for="password" class="col-sm-2 control-label">Passwort</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Passwort eingeben">
                                        @if ($errors->has('password'))
                                            <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                    <label for="password_confirmation" class="col-sm-2 control-label">Passwort Bestätigen</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Passwort bestätigen">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-3">
                                    <button type="submit" class="btn btn-success btn-block margin-bottom-1 btn-save">
                                        <i class="fa fa-fw fa-save" aria-hidden="true"></i>
                                        Speichern
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('template_linked_js')
    <script src="{{ asset('/plugins/select2/select2.full.min.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            $(".select2").select2();
        });
    </script>
@endsection