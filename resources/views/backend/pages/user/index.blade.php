@extends('backend.layouts.layout')

@section('title', 'User Index')

@section('content-header')
<section class="content-header">
    <h1>
        User
        <small>Index</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="active"><a href="{{ route('admin.user.index') }}"><i class="fa fa-dashboard"></i> User Index</a></li>
    </ol>
</section>
@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <i class="fa fa-dashboard">&nbsp;</i>
                        User Index
                    </h3>
                </div>
                <div class="box-body">
                    <fieldset>
                        <legend>Alle User</legend>

                        @if($Users->count() > 0)
                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Registriert am:</th>
                                <th colspan="2">Aktionen</th>
                            </tr>
                            @foreach($Users as $key => $value)
                                <tr>
                                    <td>{{ $value->id}}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>
                                        <a href="{{ route('admin.user.profile', $value->id) }}" class="btn btn-sm btn-info btn-block">
                                            <i class="fa fa-pencil">&nbsp;</i>
                                            Mehr Informationen
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.user.delete', $value->id) }}" class="btn btn-sm btn-danger btn-block">
                                            <i class="fa fa-pencil">&nbsp;</i>
                                            Löschen (force)
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        @else
                            <p>Keine Daten gefunden.</p>
                        @endif

                    </fieldset>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        {{ $Users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection