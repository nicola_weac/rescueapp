@extends('backend.layouts.layout')

@section('title', 'User Trackings')

@section('content-header')
    <section class="content-header">
        <h1>
            User
            <small>Trackings</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.user.index') }}"><i class="fa fa-dashboard"></i> User Index</a></li>
            <li class="active"><a href="{{ route('admin.user.profile', $User->id) }}"><i class="fa fa-dashboard"></i> User Trackings</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-3">

                <!-- User Profile Image -->
                <div class="box">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive " src="{{ $User->media()->get()->last()->url or '' }}" alt="User profile picture">
                        <h3 class="profile-username text-center">{{ $User->name }}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Navigation</h3>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav  nav-stacked">
                            <li><a href="{{ route('admin.user.profile', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Profil</a></li>
                            <li><a href="{{ route('admin.user.password', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Passwort ändern</a></li>
                            <li class="active"><a href="{{ route('admin.user.trackings', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Trackings</a></li>
                            <a href="{{ route('admin.user.delete', $User->id) }}" onclick="return confirm('Wirklich löschen?');" class="btn btn-block btn-danger"><i class="fa fa-trash"></i> User löschen</a>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <div class="col-md-9">
                <form role="form" class="form-horizontal" method="POST" action="{{ route('admin.user.profile.save', $User->id) }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                <i class="fa fa-dashboard">&nbsp;</i>
                                User Trackings
                            </h3>
                        </div>
                        <div class="box-body">
                            <fieldset>
                                <legend>User Trackings</legend>

                                @if($Trackings->count() > 0)
                                <table class="table table-bordered table-responsive">
                                    <tr>
                                        <th>ID</th>
                                        <th>Typ</th>
                                        <th>Benachrichtigungs-Periode</th>
                                        <th>Status</th>
                                        <th>Aktionen</th>
                                    </tr>
                                    @foreach($Trackings as $key => $value)
                                        <tr>
                                            <td>{{ $value->id }}</td>
                                            @if($value->type == 'escort')
                                                <td>Route</td>
                                            @else
                                                <td>Freies fahren</td>
                                            @endif
                                            <td>{{ $value->notify_period }}</td>
                                            @if($value->status == 1)
                                                <td>Beendet</td>
                                            @else
                                                <td>Aktiv</td>
                                            @endif
                                            <td>
                                                <a href="{{ route('admin.tracking.info', $value->id) }}" class="btn btn-info btn-block btn-sm">
                                                    <i class="fa fa-info">&nbsp;</i>
                                                    Mehr Informationen
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                @else
                                    <p>Keine Daten gefunden.</p>
                                @endif


                            </fieldset>
                        </div>
                        <div class="box-footer">
                           <div class="pull-right">
                               {{ $Trackings->links() }}
                           </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection