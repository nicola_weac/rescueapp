@extends('backend.layouts.layout')

@section('title', 'User Profil')

@section('content-header')
    <section class="content-header">
        <h1>
            User
            <small>Profil</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.user.index') }}"><i class="fa fa-dashboard"></i> User Index</a></li>
            <li class="active"><a href="{{ route('admin.user.profile', $User->id) }}"><i class="fa fa-dashboard"></i> User Profil</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-3">

                <!-- User Profile Image -->
                <div class="box">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive " src="{{ $User->media()->get()->last()->url or '' }}" alt="User profile picture">
                        <h3 class="profile-username text-center">{{ $User->name }}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Navigation</h3>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav  nav-stacked">
                            <li class="active"><a href="{{ route('admin.user.profile', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Profil</a></li>
                            <li><a href="{{ route('admin.user.password', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Passwort ändern</a></li>
                            <li><a href="{{ route('admin.user.trackings', $User->id) }}"><i class="fa fa-circle-o text-red"></i> Trackings</a></li>
                            <a href="{{ route('admin.user.delete', $User->id) }}" onclick="return confirm('Wirklich löschen?');" class="btn btn-block btn-danger"><i class="fa fa-trash"></i> User löschen</a>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <div class="col-md-9">
                <form role="form" class="form-horizontal" method="POST" action="{{ route('admin.user.profile.save', $User->id) }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                <i class="fa fa-dashboard">&nbsp;</i>
                                User Index
                            </h3>
                        </div>
                        <div class="box-body">
                            <fieldset>
                                <legend>User Profil</legend>

                                <div class="form-group{{ $errors->has('firstname') ? ' has-error ' : '' }}">
                                    <label for="firstname" class="control-label col-sm-2">Vorname</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="Vorname eingeben" value="{{ old('firstname', $UserProfile->firstname) }}">
                                        @if($errors->has('firstname'))
                                            <span class="help-block"> <strong>{{ $errors->first('firstname') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                    <label for="lastname" class="control-label col-sm-2">Nachname</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Nachname eingeben" value="{{ old('lastname', $UserProfile->lastname) }}">
                                        @if($errors->has('lastname'))
                                            <span class="help-block"> <strong>{{ $errors->first('lastname') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('zip') ? ' has-error ' : '' }}">
                                    <label for="zip" class="control-label col-sm-2">Postleitzahl</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="zip" name="zip" class="form-control" placeholder="PLZ eingeben" value="{{ old('zip', $UserProfile->zip) }}">
                                        @if($errors->has('zip'))
                                            <span class="help-block"> <strong>{{ $errors->first('zip') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('street') ? ' has-error ' : '' }}">
                                    <label for="street" class="control-label col-sm-2">Straße</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="street" name="street" class="form-control" placeholder="Straße eingeben" value="{{ old('street', $UserProfile->street) }}">
                                        @if($errors->has('street'))
                                            <span class="help-block"> <strong>{{ $errors->first('street') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('city') ? ' has-error ' : '' }}">
                                    <label for="city" class="control-label col-sm-2">Stadt</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="city" name="city" class="form-control" placeholder="Stadt eingeben" value="{{ old('city', $UserProfile->city) }}">
                                        @if($errors->has('city'))
                                            <span class="help-block"> <strong>{{ $errors->first('city') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>


                            </fieldset>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-6 col-xs-offset-3">
                                <button type="submit" class="btn btn-success btn-block">
                                    <i class="fa fa-save">&nbsp;</i>
                                    Speichern
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection