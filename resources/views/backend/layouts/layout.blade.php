<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'GetMe') }} - @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @if (config('webpush.gcm.sender_id'))
        <link rel="manifest" href="/manifest.json">
    @endif
    <script>
        window.Laravel = {!! json_encode([
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
            'vapidPublicKey' => config('webpush.vapid.public_key'),
            'pusher' => [
                'key' => config('broadcasting.connections.pusher.key'),
                'cluster' => config('broadcasting.connections.pusher.options.cluster'),
            ],
        ]) !!};
    </script>
    <!-- css Core -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">--}}
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">--}}

@yield('template_linked_css')

<!-- Theme style -->
    <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('css/skins/skin-purple.min.css') }}" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

@include('backend.navbar')

<!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
@include('backend.navigation')

<!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @yield('content-header')

    <!-- Main content -->
    @yield('content')
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div id="error_message"></div>



    <script>



    </script>


@include('backend.footer')

<!-- Control Sidebar -->
{{--<aside class="control-sidebar control-sidebar-dark">--}}
{{--<!-- Create the tabs -->--}}
{{--<ul class="nav nav-tabs nav-justified control-sidebar-tabs">--}}
{{--<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>--}}

{{--<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>--}}
{{--</ul>--}}
{{--<!-- Tab panes -->--}}
{{--<div class="tab-content">--}}
{{--<!-- Home tab content -->--}}
{{--<div class="tab-pane" id="control-sidebar-home-tab">--}}
{{--<h3 class="control-sidebar-heading">Recent Activity</h3>--}}
{{--<ul class="control-sidebar-menu">--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<i class="menu-icon fa fa-birthday-cake bg-red"></i>--}}

{{--<div class="menu-info">--}}
{{--<h4 class="control-sidebar-subheading">Langdon's Birthday</h4>--}}

{{--<p>Will be 23 on April 24th</p>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<i class="menu-icon fa fa-user bg-yellow"></i>--}}

{{--<div class="menu-info">--}}
{{--<h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>--}}

{{--<p>New phone +1(800)555-1234</p>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<i class="menu-icon fa fa-envelope-o bg-light-blue"></i>--}}

{{--<div class="menu-info">--}}
{{--<h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>--}}

{{--<p>nora@example.com</p>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<i class="menu-icon fa fa-file-code-o bg-green"></i>--}}

{{--<div class="menu-info">--}}
{{--<h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>--}}

{{--<p>Execution time 5 seconds</p>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--<!-- /.control-sidebar-menu -->--}}

{{--<h3 class="control-sidebar-heading">Tasks Progress</h3>--}}
{{--<ul class="control-sidebar-menu">--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<h4 class="control-sidebar-subheading">--}}
{{--Custom Template Design--}}
{{--<span class="label label-danger pull-right">70%</span>--}}
{{--</h4>--}}

{{--<div class="progress progress-xxs">--}}
{{--<div class="progress-bar progress-bar-danger" style="width: 70%"></div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<h4 class="control-sidebar-subheading">--}}
{{--Update Resume--}}
{{--<span class="label label-success pull-right">95%</span>--}}
{{--</h4>--}}

{{--<div class="progress progress-xxs">--}}
{{--<div class="progress-bar progress-bar-success" style="width: 95%"></div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<h4 class="control-sidebar-subheading">--}}
{{--Laravel Integration--}}
{{--<span class="label label-warning pull-right">50%</span>--}}
{{--</h4>--}}

{{--<div class="progress progress-xxs">--}}
{{--<div class="progress-bar progress-bar-warning" style="width: 50%"></div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript:void(0)">--}}
{{--<h4 class="control-sidebar-subheading">--}}
{{--Back End Framework--}}
{{--<span class="label label-primary pull-right">68%</span>--}}
{{--</h4>--}}

{{--<div class="progress progress-xxs">--}}
{{--<div class="progress-bar progress-bar-primary" style="width: 68%"></div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--<!-- /.control-sidebar-menu -->--}}

{{--</div>--}}
{{--<!-- /.tab-pane -->--}}
{{--<!-- Stats tab content -->--}}
{{--<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>--}}
{{--<!-- /.tab-pane -->--}}
{{--<!-- Settings tab content -->--}}
{{--<div class="tab-pane" id="control-sidebar-settings-tab">--}}
{{--<form method="post">--}}
{{--<h3 class="control-sidebar-heading">General Settings</h3>--}}

{{--<div class="form-group">--}}
{{--<label class="control-sidebar-subheading">--}}
{{--Report panel usage--}}
{{--<input type="checkbox" class="pull-right" checked>--}}
{{--</label>--}}

{{--<p>--}}
{{--Some information about this general settings option--}}
{{--</p>--}}
{{--</div>--}}
{{--<!-- /.form-group -->--}}

{{--<div class="form-group">--}}
{{--<label class="control-sidebar-subheading">--}}
{{--Allow mail redirect--}}
{{--<input type="checkbox" class="pull-right" checked>--}}
{{--</label>--}}

{{--<p>--}}
{{--Other sets of options are available--}}
{{--</p>--}}
{{--</div>--}}
{{--<!-- /.form-group -->--}}

{{--<div class="form-group">--}}
{{--<label class="control-sidebar-subheading">--}}
{{--Expose author name in posts--}}
{{--<input type="checkbox" class="pull-right" checked>--}}
{{--</label>--}}

{{--<p>--}}
{{--Allow the user to show his name in blog posts--}}
{{--</p>--}}
{{--</div>--}}
{{--<!-- /.form-group -->--}}

{{--<h3 class="control-sidebar-heading">Chat Settings</h3>--}}

{{--<div class="form-group">--}}
{{--<label class="control-sidebar-subheading">--}}
{{--Show me as online--}}
{{--<input type="checkbox" class="pull-right" checked>--}}
{{--</label>--}}
{{--</div>--}}
{{--<!-- /.form-group -->--}}

{{--<div class="form-group">--}}
{{--<label class="control-sidebar-subheading">--}}
{{--Turn off notifications--}}
{{--<input type="checkbox" class="pull-right">--}}
{{--</label>--}}
{{--</div>--}}
{{--<!-- /.form-group -->--}}

{{--<div class="form-group">--}}
{{--<label class="control-sidebar-subheading">--}}
{{--Delete chat history--}}
{{--<a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>--}}
{{--</label>--}}
{{--</div>--}}
{{--<!-- /.form-group -->--}}
{{--</form>--}}
{{--</div>--}}
{{--<!-- /.tab-pane -->--}}
{{--</div>--}}
{{--</aside>--}}
<!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    {{--<div class="control-sidebar-bg"></div>--}}
</div>
<!-- ./wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('/js/app.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/plugins/fastclick/fastclick.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBv-ansXKd1wpA04094OSa03hs-k1fe02Q&libraries=places"></script>


@yield('template_linked_js')

<script>
    $( document ).ready(function() {
        var date = '';
        window.setInterval(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            $.ajax({
                method: "GET",
                url: "/admin/dashboard/new?date="+date,
            }).done(function( data ) {
                if(data != "") {
                    var input = "";
                    $(".inserts .ok .inner").html("");
                    if(data.data.ok != ""){
                        $.each(data.data.ok, function(i, item) {
                            input = input +
                                "<div class='col-md-4'><div class=\"small-box  "+item["bg"]+"\">\n" +
                                "                                            <a href='/admin/tracking/"+item["id"]+"/info' style=\"position: absolute;color:#fff;right:0px;margin-right: 10px;margin-top: 10px; font-weight:bold;\">beenden</a>\n" +
                                "                                            <div class=\"inner\">\n" +
                                "                                                    <p style=\"font-size: 26px;\">\n" +
                                "                                                        <b>"+item["type"]+":</b> <br>\n" +
                                "                                                        "+item["name"]+"\n" +
                                "                                                    </p>\n" +

                                "                                                <p>Von: "+item["route_von"]+"</p>\n" +
                                "                                                <p>Bis: "+item["route_bis"]+"</p>\n" +
                                "                                            </div>\n" +
                                "                                            <div class=\"icon\">\n" +
                                "                                                <i class=\"fa fa-\"></i>\n" +
                                "                                            </div>\n" +
                                "                                            <a href='/admin/tracking/"+item["id"]+"/info' class=\"small-box-footer\">\n" +
                                "                                                Alarm annehmen <i class=\"fa fa-arrow-circle-right\"></i>\n" +
                                "                                            </a>\n" +
                                "                                        </div></div>";
                            // alert(item.PageName);
                        });

                        $(".inserts .ok .inner").html(input);
                        // alert(item.PageName);

                    };

                    var input = "";
                    $(".errors-footer").html("");
                    $(".inserts .errors .inner").html("");
                    if(data.data.error != ""){
                        $.each(data.data.error, function(i, item) {
                            input = input +
                                "<div class='col-md-12'><div class=\"small-box "+item["bg"]+"\">\n" +
                                "                                            <a href='/admin/tracking/"+item["id"]+"/info' style=\"position: absolute;color:#fff;right:0px;margin-right: 10px;margin-top: 10px; font-weight:bold;\">beenden</a>\n" +
                                "                                            <div class=\"inner\">\n" +
                                "                                                    <p style=\"font-size: 26px;\">\n" +
                                "                                                        <b>"+item["type"]+":</b> <br>\n" +
                                "                                                        "+item["name"]+"\n" +
                                "                                                    </p>\n" +

                                "                                                <p>Von: "+item["route_von"]+"</p>\n" +
                                "                                                <p>Bis: "+item["route_bis"]+"</p>\n" +
                                "                                            </div>\n" +
                                "                                            <div class=\"icon\">\n" +
                                "                                                <i class=\"fa fa-\"></i>\n" +
                                "                                            </div>\n" +
                                "                                            <a href='/admin/tracking/"+item["id"]+"/info' class=\"small-box-footer\">\n" +
                                "                                                Alarm annehmen <i class=\"fa fa-arrow-circle-right\"></i>\n" +
                                "                                            </a>\n" +
                                "                                        </div></div>";
                            // alert(item.PageName);
                        });

                        if(typeof $(".inserts .errors .inner").html() != "undefined"){
                            $(".inserts .errors .inner").html(input);
                        }else{
                            $(".errors-footer").html(input);
                        }


                        // alert(item.PageName);

                        if($("#error_message").html() == "" || data.data.meldung == "") {
                            $("#error_message").html(data.data.meldung);
                        }
                        // $(".inserts").html(data.data);
                        // date = data.date;
                    };
                }
            }).fail(function( ) {

            });
        }, 5000);

        $(".changer").change(function(){
            if($(this).val() == ""){
                $(".bg-green-gradient, .bg-yellow-gradient, .bg-red-gradient").parent().show();
            }else if($(this).val() == "bg-green-gradient"){
                $(".bg-yellow-gradient, .bg-red-gradient").parent().hide();
                $(".bg-green-gradient").parent().show();
            }else if($(this).val() == "bg-yellow-gradient"){
                $(".bg-green-gradient, .bg-red-gradient").parent().hide();
                $(".bg-yellow-gradient").parent().show();
            }else if($(this).val() == "bg-red-gradient"){
                $(".bg-green-gradient, .bg-yellow-gradient").parent().hide();
                $(".bg-red-gradient").parent().show();
            }
        });
    });
</script>

<div class="errors-footer" style="position:absolute;bottom: 0; right: 0;width:300px;"></div>

<!-- AdminLTE App -->
<script src="{{ asset('/js/adminlte/app.min.js') }}"></script>


</body>
</html>
