<?php

namespace App\Console;

use App\Helper\CronjobHelper;
use App\Http\Controllers\Api\EmergencyController;
use App\Models\Tracking;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Response;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call('App\Helper\CronjobHelper@checkTrackings')->everyMinute();

      ///  $schedule->call('App\Helper\CronjobHelper@checkTrackingFriends')->everyMinute();

        $schedule->call('App\Helper\CronjobHelper@checkIfTrackingIsStarted')->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
