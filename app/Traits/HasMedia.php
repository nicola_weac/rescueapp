<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

trait HasMedia {
    /** @var  File $file */
    protected $file;
    protected $media;
    protected $group;
    protected $type;
    protected $options;
    protected $filename_orig;
    protected $filename_new;
    protected $public_path;
    protected $files_directory;
    protected $files_tmp_directory;
    protected $directory;
    protected $directory_tmp;
    protected $directory_uri;
    protected $create_sub_directories;
    protected $storageKey;

    private function setup() {
        $this->public_path = rtrim(config('media.public_path'), '/\\') . '/';
        $this->files_directory = rtrim(ltrim(config('media.files_directory'), '/\\'), '/\\') . '/';
        $this->files_tmp_directory = rtrim(ltrim(config('media.files_tmp_directory'), '/\\'), '/\\') . '/';
        $this->create_sub_directories = config('media.sub_directories');
        $this->directory = $this->public_path . $this->files_directory;
        $this->directory_tmp = $this->public_path . $this->files_tmp_directory;
        if($this->create_sub_directories) {
            $this->directory_uri = Str::lower(class_basename($this)) . '/' . $this->id . '/';
        }
        $this->directory .= $this->directory_uri;
        if(!empty($this->file)) {
            $this->filename_orig = $this->file->getClientOriginalName();
            $this->filename_new = $this->getFilename();
        }
    }

    public function media() {
        return $this->morphMany(config('media.model'), 'mediable')->where('group', 'default');
    }

    public function mediaCover() {
        return $this->morphMany(config('media.model'), 'mediable')->where('group', 'cover');
    }

    public function saveMedia($file, $group = 'default', $type = 'single', $options = []) {

        $this->file = $file;
        $this->group = $group;
        $this->type = $type;
        $this->options = $options;

        $this->setup();
        $this->parseOptions();

        if($this->type == 'single') {
            $this->removeExistingMedia();
        }
        $result = $this->databasePut();
        $this->storagePut();

        return $result;
    }

    public function saveTmpMedia($file, $group = 'default', $type = 'single', $storageKey = '', $options = []) {
        $this->file = $file;
        $this->group = $group;
        $this->type = $type;
        $this->options = $options;
        $this->storageKey = $storageKey;

        $this->setup();
        $this->parseOptions();

        if($this->type == 'single') {
            //$this->removeExistingTmpMedia();
        }

        $result = $this->databaseTmpPut();
        $this->storageTmpPut();

        return $result;
    }

    public function updateMediaById($id, $options) {
        $this->options = $options;
        $this->parseOptions();
        $model = config('media.model');
        $this->media = $model::find($id);
        $this->media->alt = $this->getAlt();
        $this->media->title = $this->getTitle();
        $this->media->name = $this->getName();
        $this->media->weight = $this->getWeight();
        $this->media->save();
    }

    public function getMedia($group = NULL) {
        if(is_null($group)) {
            return $this->media()->orderBy('weight', 'ASC')->get();
        }
        return $this->getMediaByGroup($group);
    }

    private function getMediaByGroup($group) {
        return $this->media()
            ->where('group', $group)
            ->orderBy('weight', 'ASC')
            ->get();
    }

    private function getMediaById($id) {
        return $this->media()->find($id);
    }

    public function deleteMedia($group = NULL) {
        $media = $this->getMedia($group);
        $count  = 0;
        foreach($media as $item) {
            $count += (int) $this->removeMedia($item);
        }
        return $count;
    }

    public function deleteMediaById($id) {
        $media = $this->getMediaById($id);
        if(!is_null($media)) {
            return $this->removeMedia($media);
        }
        return false;
    }

    private function deleteMediaByGroup($group) {
        return $this->media()->where('group', $group)->delete();
    }

    private function removeMedia($media) {
        $this->setup();
        if(File::delete($this->public_path . $this->files_directory . $media->filename)) {
            $media->delete();
            return true;
        }
        return false;
    }

    private function parseOptions() {
        $default_options = [
            'alt' => null,
            'title' => null,
            'name' => null,
            'weight' => null,
        ];
        $this->options += $default_options;
        $this->options = (object) $this->options;
    }

    private function getFileName() {
        switch(config('media.rename')) {
            case 'transliterate': {
                $this->filename_new = $this->filename_orig;
            } break;
            case 'unique': {
                $this->filename_new = md5(microtime() . str_random(5)) . '.' . $this->filename_orig;
            } break;
            case 'nothing': {
                $this->filename_new = $this->file->getClientOriginalName();
            } break;
        }
        return $this->fileExistsRename();
    }

    private function fileExistsRename() {
        if(!File::exists($this->directory . $this->filename_new)) {
            return $this->filename_new;
        }
        return $this->fileRename();
    }

    private function fileRename() {
        $filename = $this->filename_new;
        $ext = '.' . File::extension($this->filename_new);
        $basename = rtrim($filename, $ext);
        $increment = 0;
        while(File::exists($this->directory . $filename)) {
            $filename = $basename . '_' . ++$increment . $ext;
        }
        return $this->filename_new = $filename;
    }

    private function getAlt() {
        if(!is_null($this->options->alt)) {
            return $this->options->alt;
        }
        if(!is_null($this->media)) {
            return $this->media->alt;
        }
        return '';
    }

    private function getTitle() {
        if(!is_null($this->options->title)) {
            return $this->options->title;
        }
        if(!is_null($this->media)) {
            return $this->media->title;
        }
        return '';
    }

    private function getName() {
        if(!is_null($this->options->name)) {
            return $this->options->name;
        }
        if(!is_null($this->media)) {
            return $this->media->name;
        }
        return '';
    }

    private function getWeight() {
        if(!is_null($this->options->weight)) {
            return $this->options->weight;
        }
        if(!is_null($this->media)) {
            return $this->media->weight;
        }
        return $this->media()->where('group', $this->group)->count();
    }

    private function databasePut() {
        $media = [
            'filename' => $this->directory_uri . $this->filename_new,
            'mime' => $this->file->getMimeType(),
            'size' => $this->file->getSize(),
            'title' => $this->getTitle(),
            'alt' => $this->getAlt(),
            'name' => ($this->getName() == '') ? $this->file->getClientOriginalName() : $this->getName() ,
            'group' => $this->group,
            'status' => true,
            'weight' => $this->getWeight(),
        ];
        $model = config('media.model');

        $test =$this->media()->save(new $model($media));

        return $test;
    }

    private function databaseTmpPut() {
        $media = [
            'tmpKey' => $this->storageKey,
            'filename' => $this->directory_uri . $this->filename_new,
            'mime' => $this->file->getMimeType(),
            'size' => $this->file->getSize(),
            'title' => $this->getTitle(),
            'alt' => $this->getAlt(),
            'name' => ($this->getName() == '') ? $this->file->getClientOriginalName() : $this->getName() ,
            'group' => $this->group,
            'status' => true,
            'weight' => $this->getWeight(),
        ];
        $model = config('media.model');

        $test = (new $model)->create($media);

        return $test;
    }

    private function removeExistingMedia() {
        $exist_media = $this->getMedia($this->group);
        if(!$exist_media->isEmpty()) {
            return $this->deleteMedia($this->group);
        }
        return 0;
    }

    private function storageTmpPut() {
        if($this->makeDirectory($this->directory_tmp)) {
            $this->file->move($this->directory_tmp, $this->filename_new);
        }
    }

    private function storagePut() {
        if($this->makeDirectory($this->directory)) {
            $this->file->move($this->directory, $this->filename_new);
        }
    }

    private function storageClone() {
        if($this->makeDirectory($this->directory)) {
            return File::copy($this->public_path . $this->files_directory . $this->media->filename, $this->directory . $this->filename_new);
        }
        return false;
    }

    private function makeDirectory($directory) {
        if(File::isDirectory($directory)) {
            return true;
        }
        return File::makeDirectory($directory, 0755, true);
    }

    public function cloneMedia($media, $clone_storage = false, $clone_attributes = []) {
        $this->media = $media->replicate();
        $this->setup();
        $this->filename_new = basename($media->filename);
        if ($clone_storage) {
            $this->fileExistsRename();
            $this->storageClone();
        }
        $this->media->fill($clone_attributes);
        $this->media->filename = $this->directory_uri . $this->filename_new;
        return $this->media()->save($this->media);
    }
}