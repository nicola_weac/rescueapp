<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingStartedNotification extends Model
{
    protected $table = 'tracking_started_notifications';

    protected $fillable = [
        'user_id', 'tracking_id', 'requested_user_id', 'active'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
