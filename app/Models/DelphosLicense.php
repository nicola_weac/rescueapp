<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DelphosLicense extends Model
{
    protected $table = 'delphos_license';

    protected $fillable = [
        'user_id', 'hasLicense', 'expiry_date'
    ];

    protected $dates = [
        'expiry_date', 'created_at', 'updated_at'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
