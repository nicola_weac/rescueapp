<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactTracking extends Model
{

    use SoftDeletes;

    protected $table = 'contact_tracking';

    protected $fillable = [
        'tracking_id', 'contact_id', 'status', 'cancelled'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function tracking(){
        return $this->hasOne(Tracking::class, 'id', 'tracking_id');
    }
}
