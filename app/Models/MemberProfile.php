<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberProfile extends Model
{
    protected $table = 'members_profile';

    protected $fillable = [
        'firstname', 'lastname', 'street', 'zip', 'city'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
