<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profile';

    /*
     * @toDo: https://laravel.com/docs/5.6/encryption -> persönliche Daten
     */
    protected $fillable = [
        'firstname', 'lastname', 'street', 'city', 'zip', 'user_id', 'phone', 'country', 'age', 'gender', 'height',
        'intolerances', 'pre_existing_conditions', 'medication', 'blood_type'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
