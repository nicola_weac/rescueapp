<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FacebookUsers extends Model
{
    protected $table = 'facebook_users';

    protected $fillable = [
        'user_id', 'provider_user_id', 'provider'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
