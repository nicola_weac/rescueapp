<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'position';

    protected $fillable = [
        'user_id', 'tracking_id', 'lat', 'lng', 'street', 'zip', 'city'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
