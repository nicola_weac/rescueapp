<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmergencyFile extends Model
{
    protected $table = 'emergency_files';

    protected $fillable = [
        'user_id', 'emergency_id', 'path', 'mime', 'size', 'battery_lvl', 'battery_state'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
