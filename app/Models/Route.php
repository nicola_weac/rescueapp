<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'routes';

    protected $fillable = [
        'tracking_id', 'from_lat', 'to_lat', 'from_lng', 'to_lng',
        'from_city', 'to_city', 'from_street', 'to_street', 'from_zip', 'to_zip'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
