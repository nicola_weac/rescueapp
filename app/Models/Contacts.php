<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contact';

    protected $fillable = [
        'sender_id', 'recipient_id', 'status'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'recipient_id');
    }

    public function trackings(){
        return $this->hasMany(ContactTracking::class, 'contact_id', 'id');
    }

    public function emergencies(){
        return $this->hasMany(ContactEmergency::class, 'contact_id', 'id');
    }
}
