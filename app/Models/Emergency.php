<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emergency extends Model
{
    protected $table = 'emergencies';

    protected $fillable = [
        'user_id', 'police', 'ambulance', 'guardHelp', 'lat', 'lng', 'delphos', 'tracking_id'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function tracking(){
        return $this->belongsTo(Tracking::class, 'tracking_id', 'id');
    }

    public function emergencyContact(){
        return $this->hasMany(ContactEmergency::class, 'emergency_id', 'id');
    }

    public function files(){
        return $this->hasMany(EmergencyFile::class, 'emergency_id', 'id');
    }
}
