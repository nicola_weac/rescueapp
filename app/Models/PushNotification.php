<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{
    protected $table = 'push_notification';

    protected $fillable = [
        'user_id', 'device_id', 'os', 'type'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
