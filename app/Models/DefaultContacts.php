<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DefaultContacts extends Model
{
    protected $table = 'default_contact';

    protected $fillable = [
        'user_id', 'default_contact_id'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function defaultUser(){
        return $this->hasOne(Contacts::class, 'id', 'default_contact_id');
    }
}
