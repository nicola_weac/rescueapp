<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use NotificationChannels\WebPush\HasPushSubscriptions;

class Members extends Authenticatable
{
    use Notifiable, HasPushSubscriptions;
    protected $table = 'members';

    protected $fillable = [
        'name', 'email', 'password'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
