<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordResetCodes extends Model
{
    protected $table = 'password_reset_codes';

    protected $fillable = [
        'user_id', 'email', 'code', 'expiry_date'
    ];

    protected $dates = [
        'expiry_date', 'created_at', 'updated_at'
    ];
}
