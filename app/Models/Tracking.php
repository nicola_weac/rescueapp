<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table = 'tracking';

    protected $fillable = [
        'user_id', 'type', 'notify_period', 'delphos', 'status', 'update_countdown', 'friendinfo'
    ];

    protected $dates = [
        'created_at', 'updated_t'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function userprofile(){
        return $this->hasOne(UserProfile::class, 'user_id', 'user_id');
    }

    public function route(){
        return $this->hasOne(Route::class, 'tracking_id', 'id');
    }

    public function emergency(){
        return $this->hasOne(Emergency::class, 'tracking_id', 'id');
    }

    public function position(){
        return $this->hasMany(Position::class, 'tracking_id', 'id');
    }

    public function trackingContact(){
        return $this->hasMany(ContactTracking::class, 'tracking_id', 'id');
    }

    public function contacts(){
        return $this->hasManyThrough(Contacts::class, ContactTracking::class, 'tracking_id', 'id', 'id', 'contact_id');
    }

    public function alive(){
        return $this->hasMany(ClientAlive::class, 'tracking_id', 'id');
    }

    public function battery(){
        return $this->hasMany(TrackingPhoneBattery::class, 'tracking_id', 'id');
    }

}
