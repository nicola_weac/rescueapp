<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingPhoneBattery extends Model
{
    protected $table = 'tracking_phone_battery';

    protected $fillable = [
        'tracking_id', 'battery_lvl', 'battery_state'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
