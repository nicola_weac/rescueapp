<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmergencyToDo extends Model
{
    protected $table = 'emergency_to_dos';

    protected $fillable = [
        'user_id', 'name', 'type', 'sort_id', 'active'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
