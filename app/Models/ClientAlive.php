<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientAlive extends Model
{
    protected $table = 'client_alive';

    protected $fillable = [
        'user_id', 'tracking_id'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
