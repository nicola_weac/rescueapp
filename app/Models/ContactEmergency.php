<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactEmergency extends Model
{
    protected $table = 'contact_emergency';

    protected $fillable = [
        'emergency_id', 'contact_id', 'status'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function emergency(){
        return $this->hasOne(Emergency::class, 'id', 'emergency_id');
    }
}
