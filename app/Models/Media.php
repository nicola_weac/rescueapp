<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Media extends Model
{
    protected $table = 'media';

    protected $fillable = [
        'Url', 'filename', 'mime', 'size', 'name', 'alt', 'title', 'group', 'status', 'weight'
    ];

    public function mediable() {
        return $this->morphTo()->where('group', 'default');
    }

    public function getUrlAttribute() {
        return URL::asset(config('media.files_directory') . $this->filename);
    }

    public function getTitleAttribute($value) {
        if(empty($value)) {
            return basename($this->attributes['filename']);
        }
        return $value;
    }
}
