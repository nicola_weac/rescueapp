<?php
/**
 * Created by PhpStorm.
 * User: bendixnicola
 * Date: 07.05.18
 * Time: 10:29
 */
namespace App\Helper;

use App\Models\Emergency;
use App\Models\TrackingStartedNotification;
use App\Models\Tracking;
use App\Http\Controllers\Api\EmergencyController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use App\Models\PushNotification;

class CronjobHelper
{
    public function checkIfTrackingIsStarted(){

        $Trackings = Tracking::where('status', 'inactive')->where('emergency', '0')->get();

        foreach ($Trackings as $key => $value){

            $checkDate = date('Y-m-d H:i:s', strtotime('+180 seconds',$value->created_at->timestamp));

            if (Carbon::now()->format('Y-m-d H:i:s') >= $checkDate){
                $value->update([
                    'status' => 'finished',
                ]);

                $notification = TrackingStartedNotification::where('tracking_id', $value->id)->first();

                if ($notification){
                    $notification->update([
                        'active' => false
                    ]);
                }

                $pushnotification = PushNotification::where('user_id', $value->user_id)->first();

                if ($pushnotification){
                    $push = new \Edujugon\PushNotification\PushNotification('apn');
                    $push->setMessage([
                        'aps' => [
                            'alert' => [
                                'title' => 'Guarding abgebrochen',
                                'body' => 'Niemand hat dein Tracking angenommen.'
                            ],
                            'sound' => 'default',
                            'badge' => 1,
                            'category' => 'delphos.noguards'
                        ],
                    ])
                        ->setDevicesToken($pushnotification->device_id)
                        ->send();

                }
            }
        }

    }

    public function checkTrackings(){

        $Trackings = Tracking::where('status', "active")->get();

        foreach ($Trackings as $key => $value){

            if (Carbon::now()->format('Y-m-d H:i:s') >= $value->update_countdown){

                if (Emergency::where('tracking_id', $value->id)->count() == 0){
                    
                    $dd = new EmergencyController();
                    $dd->sendAutoEmergency($value->id);



                }
            }
        }
    }

    public function checkTrackingFriends(){
        $Trackings = Tracking::where('delphos', 0)->where('emergency', 0)->where('status', "active")->where('friendinfo', 0)->get();

        foreach($Trackings as $item){
            $time = strtotime("+59 second",strtotime($item->created_at->format("Y-m-d H:i:s")));

            if($time < time()){
                $toDo = $item->trackingContact()->where("accepted",1)->count();

                if($toDo == 0){

                    $item->friendinfo = 1;

                    $item->update();

                    $pushown = PushNotification::where('user_id', $item->user_id)->first();
                    if ($pushown){
                        $push = new \Edujugon\PushNotification\PushNotification('apn');
                        $push->setMessage([
                            'aps' => [
                                'alert' => [
                                    'title' => 'Keine Guards vorhanden',
                                    'body' =>   'Leider habe keine Guards dein Tracking angenommen.'
                                ],
                                'sound' => 'default',
                                'badge' => 1,
                                'category' => 'delphos.noguards'
                            ],
                        ])
                            ->setDevicesToken($pushown->device_id)
                            ->send();
                    }
                }
            }

        }
    }
}