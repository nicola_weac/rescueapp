<?php

namespace App;

use App\Models\ClientAlive;
use App\Models\Contacts;
use App\Models\DelphosLicense;
use App\Models\Emergency;
use App\Models\EmergencyToDo;
use App\Models\TrackingStartedNotification;
use App\Models\Position;
use App\Models\Tracking;
use App\Models\UserProfile;
use App\Traits\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasMedia, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function contacts(){
        return $this->hasMany(Contacts::class, 'sender_id', 'id');
    }

    public function iscontact(){
        return $this->hasMany(Contacts::class, 'recipient_id', 'id');
    }

    public function trackings(){
        return $this->hasMany(Tracking::class, 'user_id', 'id')->orderBy('created_at', 'DESC');
    }

    public function activeTracking(){
        return $this->hasOne(Tracking::class, 'user_id', 'id')
            ->where('status', 'active')->orwhere('status', 'inactive');
    }

    public function emergencies(){
        return $this->hasMany(Emergency::class, 'user_id', 'id');
    }

    public function clientAlive(){
        return $this->hasMany(ClientAlive::class, 'user_id', 'id');
    }

    public function delphosLicense(){
        return $this->hasOne(DelphosLicense::class, 'user_id', 'id');
    }

    public function profile(){
        return $this->hasOne(UserProfile::class, 'user_id', 'id');
    }

    public function positions(){
        return $this->hasMany(Position::class, 'user_id', 'id');
    }

    public function emergencyToDo(){
        return $this->hasMany(EmergencyToDo::class, 'user_id', 'id');
    }

    public function trackingStartedNotifications()
    {
        return $this->hasMany(TrackingStartedNotification::class, 'requested_user_id', 'id')
            ->where('active', true);
    }

    /**
     * @ToDo:
     * https://laravel.com/docs/5.6/eloquent-mutators#accessors-and-mutators
     *
     * name + email = alles klein, name = slug
     */
}
