<?php

namespace App\Http\Controllers\Backend;

use App\Models\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class DashboardController extends Controller
{
    public function index(){
        $date = date("Y-m-d H:i:s");
        $Tracking = Tracking::where('status', 0)->where('shown', 0)->get();

        return view('backend.pages.dashboard')
            ->with('Tracking', $Tracking)
            ->with('date', $date);
    }

    public function newele(Request $request){
        $date = $request->get("date");
        $Tracking = Tracking::where('status', 0)->where('shown', 0)->get();

        $out = [];
        $out["ok"] = null;
        $out["error"] = null;
        $out["meldung"] = null;
        if($Tracking->count() > 0){
            $meldung = 0;
            foreach($Tracking as $key => $value){
                if($value->emergency == null || $value->delphos == 0){
                    $out["ok"][$key]["name"] = $value->userprofile->firstname.' '.$value->userprofile->lastname;
                    $out["ok"][$key]["id"] = $value->id;

                    if($value->emergency == null){
                        $out["ok"][$key]["bg"] = "bg-green-gradient";
                    }else{
                        $out["ok"][$key]["bg"] = "bg-yellow-gradient";
                    }
                    if($value->route()->count() > 0){
                        $out["ok"][$key]["type"] = "Route von";
                        $out["ok"][$key]["route_von"] = $value->route != null ? $value->route->from_city : null;
                        $out["ok"][$key]["route_bis"] = $value->route != null ? $value->route->to_city : null;
                    }else{
                        $out["ok"][$key]["type"] = "Überwachung von";
                        $out["ok"][$key]["route_von"] = "--";
                        $out["ok"][$key]["route_bis"] = "--";
                    }
                }else{
                    $out["error"][$key]["name"] = $value->userprofile->firstname.' '.$value->userprofile->lastname;
                    $out["error"][$key]["id"] = $value->id;
                    $out["error"][$key]["bg"] = "bg-red-gradient";
                    if($value->route()->count() > 0){
                        $out["error"][$key]["type"] = "Route von";
                        $out["error"][$key]["route_von"] = $value->route != null ? $value->route->from_city : null;
                        $out["error"][$key]["route_bis"] = $value->route != null ? $value->route->to_city : null;
                    }else{
                        $out["error"][$key]["type"] = "Überwachung von";
                        $out["error"][$key]["route_von"] = "--";
                        $out["error"][$key]["route_bis"] = "--";
                    }
                    $meldung = 1;
                }

            }

            if($meldung) {
                $out["meldung"] = '<audio autoplay="autoplay"><source src="/error.mp3" type="audio/mpeg" /><embed hidden="true" autostart="true" loop="false" src="/error.mp3" /></audio>';
            }
        }

        if(empty($out["ok"])){
            $out["ok"] = null;
        }

        if(empty($out["error"])){
            $out["error"] = null;
        }

        $date = date("Y-m-d H:i:s");
        return Response::json([
            'date' => $date,
            'data' => $out
        ], 200);
    }
    
    public function newList(Request $request){

        $date = $request->get("date");
        $Tracking = Tracking::where('status', 0)->get();

        $out = "";
        if($Tracking->count() > 0){
            $meldung = 0;
            foreach($Tracking as $key => $value){
                $out .= '<div class="col-lg-3 col-xs-12">';
                        if($value->emergency == null){
                            $out .= '<div class="small-box bg-green-gradient">';
                                $out .= '<div class="inner">';
                                if($value->route()->count() > 0){

                                        $out .= ' <p style="font-size: 26px;"><b>Route von</b> '.$value->userprofile->firstname.' '.$value->userprofile->lastname.'</p>';
                                        $out .= ' <p>Von: '.$value->route != null ? $value->route->from_city : '---' .'</p>';
                                        $out .= ' <p>Bis: '.$value->route != null ? $value->route->to_city : '---'.'</p>';
                                }else{
                                         $out .= '<p style="font-size: 26px;"><b>Freies fahren von</b> '.$value->userprofile->firstname.' '.$value->userprofile->lastname.'</p>';
                                         $out .= '<p>Von: --- </p>';
                                         $out .= '<p>Bis: ---</p>';
                                }
                                 $out .= '</div>';
                       $out .= '          <div class="icon">';
            $out .= '                         <i class="fa"></i>';
 $out .= '                                </div>';
                    $out .= '             <a href="'. route('admin.tracking.info', $value->id) .'" class="small-box-footer">';
         $out .= 'Mehr Informationen <i class="fa fa-arrow-circle-right"></i>';
                                    $out .= '</a>';
                $out .= '</div>';
                        }else{
                            if($value->delphos){$meldung = 1;}
                             $out .= '<div class="small-box '.(($value->delphos == 0) ? 'bg-yellow-gradient' : 'bg-red-gradient') .' ">';
                             $out .= '<a href="'.route('admin.tracking.end', $value->id).'" style="position: absolute;color:#fff;right:0px;margin-right: 10px;margin-top: 10px; font-weight:bold;">beenden</a>';
                                 $out .= '<div class="inner">';
                                    if($value->route()->count() > 0){
                                         $out .= '<p style="font-size: 26px;">';
                                             $out .= '<b>Route:</b> <br>';
                                             $out .= $value->userprofile->firstname.' '.$value->userprofile->lastname;
                                         $out .= '</p>';
                                    }else{
                                         $out .= '<p style="font-size: 26px;">';
                                             $out .= '<b>Freies fahren:</b> <br>';
                                             $out .= $value->userprofile->firstname.' '.$value->userprofile->lastname.'</p>';
                                     }
                                         $out .= '<p>Von: '.$value->route != null ? !empty($value->route->from_city) ? $value->route->from_city : '---' : '---' .'</p>';
                                         $out .= '<p>Bis: '. $value->route != null ?  !empty($value->route->to_city) ? $value->route->to_city : '---' : '---' .'</p>';
                                 $out .= '</div>';
                                 $out .= '<div class="icon">';
                                     $out .= '<i class="fa fa-"></i>';
                               $out .= '  </div>';
                    $out .= '             <a href="'. route('admin.tracking.info', $value->id) .'" class="small-box-footer">';
         $out .= 'Mehr Informationen <i class="fa fa-arrow-circle-right"></i>';
                                 $out .= '</a>';
                             $out .= '</div>';
        
                    $out .= '</div>';

                    }
            }


            if($meldung) {$out .='<audio autoplay="autoplay"><source src="/error.mp3" type="audio/mpeg" /><embed hidden="true" autostart="true" loop="false" src="/error.mp3" /></audio>';}
        }


        $date = date("Y-m-d H:i:s");

        return Response::json([
            'date' => $date,
            'data' => $out
        ], 200);
    }
}
