<?php

namespace App\Http\Controllers\Backend;

use App\Models\MemberProfile;
use App\Models\Members;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MembersController extends Controller
{
    public function index()
    {
        $Members = Members::paginate(15);

        return view('backend.pages.member.index')
            ->with('Members', $Members);
    }

    public function profile($memberid)
    {
        $Member = Members::find($memberid);

        $MemberProfile = MemberProfile::where('member_id', $memberid)->first();

        return view('backend.pages.member.profile')
            ->with('Member', $Member)
            ->with('MemberProfile', $MemberProfile);
    }

    public function profileSave(Request $request, $memberid){
        $v = Validator::make($request->all(), [
            'firstname' => '',
            'lastname' => '',
            'street' => '',
            'zip' => '',
            'city' => ''
        ]);

        if($v->fails()){

            return Redirect::route('admin.member.profile', $memberid)
                ->withErrors($v)
                ->withInput();

        }else{

            $UserProfile = MemberProfile::where('user_id', $memberid)->first();

            $UserProfile->firstname = $request->input('firstname');
            $UserProfile->lastname = $request->input('lastname');
            $UserProfile->street = $request->input('street');
            $UserProfile->zip = $request->input('zip');
            $UserProfile->city = $request->input('city');

            $UserProfile->save();

        }

        return Redirect::route('admin.member.profile', $memberid);
    }
}
