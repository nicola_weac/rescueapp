<?php

namespace App\Http\Controllers\Backend;

use App\Models\DelphosLicense;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class LicenseController extends Controller
{
    public function addSave(Request $request){

        $v = Validator::make($request->all(), [
            'user_id' => 'integer',
            'expiry_date' => 'required'
        ]);

        if ($v->fails()){
            return Redirect::route('admin.license.add')
                    ->withErrors($v)
                    ->withInput();
        }else{

            DelphosLicense::create([
                'user_id' => $request->input('user_id'),
                'hasLicense' => true,
                'expiry_date' => $request->input('expiry_date'),
            ]);

        }

        return Redirect::route('admin.license.list');
    }

    public function add(){

        $DelphosLicense = DelphosLicense::where('hasLicense', 0)->select('user_id')->get();

        $Users = User::whereIn('id', $DelphosLicense)->get();

        return view('backend.pages.license.add')
            ->with('Users', $Users);
    }

    public function list(){

        $Licenses = DelphosLicense::where('hasLicense', true)->get();

        return view('backend.pages.license.list')
            ->with('Licenses', $Licenses);
    }
}
