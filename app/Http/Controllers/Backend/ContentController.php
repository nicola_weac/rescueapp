<?php

namespace App\Http\Controllers\Backend;

use App\Models\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    public function editSave(Request $request, $contentid){

        $v = Validator::make($request->all(), [
            'identifier' => 'required',
            'text' => 'required',
        ]);

        if($v->fails()){
            return Redirect::route('admin.content.edit', $contentid)
                ->withErrors($v)
                ->withInput();
        }else{

            $content = Content::find($contentid);

            $content->identifier = $request->input('identifier');
            $content->text = $request->input('text');

            $content->save();

        }
        return Redirect::route('admin.content');
    }

    public function edit($contentid){

        $Content = Content::find($contentid);

        return view('backend.pages.content.edit')
            ->with('Content', $Content);
    }

    public function addSave(Request $request){

        $v = Validator::make($request->all(), [
            'identifier' => 'required',
            'text' => 'required'
        ]);

        if($v->fails()){

            return Redirect::route('admin.content.add')
                ->withErrors($v)
                ->withInput();
        }else{

            Content::create([
                'identifier' => $request->input('identifier'),
                'text' => $request->input('text')
            ]);

        }
        return Redirect::route('admin.content');
    }

    public function add(){

        return view('backend.pages.content.add');
    }

    public function showAll(){

        $Contents = Content::all();

        return view('backend.pages.content.list')
            ->with('Contents', $Contents);
    }
}
