<?php

namespace App\Http\Controllers\Backend;

use App\Models\Emergency;
use App\Models\EmergencyFile;
use App\Models\Media;
use App\Models\Position;
use App\Models\Tracking;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class TrackingController extends Controller
{
    public function showAll(){
        $trackings = Tracking::orderBy('status', 'ASC')->orderBy('created_at', 'DESC')->paginate(15);

        return view('backend.pages.tracking.index')
            ->with('Trackings', $trackings);
    }

    public function showInfo($trackingid){
        $tracking = Tracking::find($trackingid);

        $profilePicture = $tracking->user->media()->first();
        $media = [];
        if ($tracking->emergency == 1){
            $tracking->shown = 1;
            $tracking->save();
            $emergency = Emergency::where('tracking_id', $trackingid)->first();
            if($emergency) {
                $media = EmergencyFile::where('emergency_id', $emergency->id)->get();
            }
        }

        $position = Position::where('tracking_id', $trackingid)->orderByDesc("id")->get()->last();

        return view('backend.pages.tracking.info')
            ->with('position', $position)
            ->with('media', $media)
            ->with('profilePicture', $profilePicture)
            ->with('Tracking', $tracking);
    }

    public function ende($trackingid){
        $Tracking = Tracking::find($trackingid);
        $Tracking->status = 1;
        $Tracking->save();

        header("Location: /admin/dashboard");
        die();
    }

    public function showMediaAjax($trackingid){
        $tracking = Tracking::find($trackingid);
        $out = "";
        if ($tracking->emergency == 1) {
            $Emergency = Emergency::where('tracking_id', $trackingid)->first();

            $media = $Emergency->files()->orderByDesc("id")->limit(5)->get();


            foreach($media as $m){
                $out .= '
                <li>
                    <a href="javascript:void(0);" data-video="/admin/tracking/media/show/'.$m->id.'" class="file">
                        '.$m->created_at->format("d.m H:i:s").'
                    </a>
                </li>
                ';
            }



        }

        return Response::json([
            'message' => $out,
            'lat' => ($tracking->position()->count() > 0) ? $tracking->position()->first()->lat : $Emergency->lat,
            'lng' => ($tracking->position()->count() > 0) ? $tracking->position()->first()->lng : $Emergency->lng,
        ], 200);
    }

    public function showMedia($trackingid){

        $tracking = Tracking::find($trackingid);

        $profilePicture = Media::where('mediable_type', User::class)->where('mediable_id', $tracking->user->id)->first();
        if($profilePicture){
            $profilePicture = $profilePicture->url;
        }else{
            $profilePicture = '';
        }

        if ($tracking->emergency == 1){

            $Emergency = Emergency::where('tracking_id', $trackingid)->first();
            if($Emergency){
                $media = $Emergency->files;
            }else{
                $media = [];
            }
        }

        return view('backend.pages.tracking.media')
            ->with('profilePicture', $profilePicture)
            ->with('Tracking', $tracking)
            ->with('media', $media);
    }

    public function loadMedia($id){

        $media = EmergencyFile::find($id);
        $fileContents = Storage::disk('local')->get('public/' . $media->path);

        $response = Response::make($fileContents, 200);
        $response->header('Content-Type', $media->mime);
        return $response;
    }

    public function showRoute($trackingid){

        $tracking = Tracking::find($trackingid);

        $profilePicture = Media::where('mediable_type', User::class)->where('mediable_id', $tracking->user->id)->first();
        if($profilePicture){
            $profilePicture = $profilePicture->url;
        }else{
            $profilePicture = '';
        }

        $route = $tracking->route;

        return view('backend.pages.tracking.route')
            ->with('profilePicture', $profilePicture)
            ->with('Tracking', $tracking)
            ->with('route', $route);

    }
}
