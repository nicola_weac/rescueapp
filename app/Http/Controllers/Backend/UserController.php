<?php

namespace App\Http\Controllers\Backend;

use App\Models\Media;
use App\Models\Tracking;
use App\Models\UserProfile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $Users = User::paginate(15);

        return view('backend.pages.user.index')
            ->with('Users', $Users);
    }

    public function profile($userid)
    {
        $User = User::find($userid);
        $UserProfile = $User->profile;

        return view('backend.pages.user.profile')
            ->with('UserProfile', $UserProfile)
            ->with('User', $User);
    }

    public function profileSave(Request $request, $userid)
    {
        $v = Validator::make($request->all(), [
            'firstname' => '',
            'lastname' => '',
            'street' => '',
            'zip' => '',
            'city' => ''
        ]);

        if($v->fails()){

            return Redirect::route('admin.user.profile', $userid)
                ->withErrors($v)
                ->withInput();

        }else{

            $UserProfile = UserProfile::where('user_id', $userid)->first();

            $UserProfile->firstname = $request->input('firstname');
            $UserProfile->lastname = $request->input('lastname');
            $UserProfile->street = $request->input('street');
            $UserProfile->zip = $request->input('zip');
            $UserProfile->city = $request->input('city');

            $UserProfile->save();

        }

        return Redirect::route('admin.user.profile', $userid);
    }

    public function password($userid)
    {
        $User = User::find($userid);

        return view('backend.pages.user.password')
            ->with('User', $User);
    }

    public function passwordSave(Request $request, $userid)
    {
        $v = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
        ]);

        if($v->fails()){

            return Redirect::route('admin.user.password', $userid)
                ->withErrors($v)
                ->withInput();

        }else{

            $User = User::find($userid);

            $User->password = bcrypt($request->input('password'));

            $User->save();

        }

        return Redirect::route('admin.user.password', $userid);
    }

    public function delete($userid){

        $User = User::with([
            'defaultContact',
            'contacts',
            'iscontact',
            'positions',
            'trackings',
            'trackings.route',
            'emergencies',
            'emergencies.files',
            'clientalive',
            'delphosLicense',
            'profile'
        ])->find($userid);

        if ($User){
            $User->defaultContact()->delete();
            $User->contacts()->delete();
            $User->iscontact()->delete();
            $User->positions()->delete();
            foreach ($User->trackings as $key => $value){
                $value->route()->delete();
            }
            foreach ($User->emergencies as $key => $value){
                $value->files()->delete();
            }
            $User->emergencies()->delete();
            $User->clientalive()->delete();
            $User->trackings()->delete();
            $User->delphosLicense()->delete();
            $User->profile()->delete();

            $User->name = 'anonymus';
            $User->email = 'anonymus';
            $User->save();

            $User->delete();
        }

        return Redirect::route('admin.user.index');
    }

    public function trackings($userid){

        $User = User::find($userid);
        $Trackings = $User->trackings()->paginate(15);

        return view('backend.pages.user.trackings')
            ->with('User', $User)
            ->with('Trackings', $Trackings);
    }
}
