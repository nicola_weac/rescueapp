<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\EmergencyToDoResource;
use App\Models\EmergencyToDo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class EmergencyToDoController extends Controller
{
    public function showToDo(){

        $authUser = Auth::guard('api')->user();

        return EmergencyToDoResource::collection($authUser->emergencyToDo()->orderBy('sort_id', 'ASC')->get());
    }

    public function update(Request $request){

        $v = Validator::make($request->all(), [
            'order' => 'array'
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->errors(),
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();

            foreach ($request->input('todo') as $key => $value){

                if ($key == 'callme'){
                    EmergencyToDo::where('user_id', $authUser->id)
                        ->where('type', 'callme')
                        ->update(['sort_id' => $value['sort'], 'active' => $value['active']]);
                }
                if ($key == 'callcontact'){
                    EmergencyToDo::where('user_id', $authUser->id)
                        ->where('type', 'callcontact')
                        ->update(['sort_id' => $value['sort'], 'active' => $value['active']]);
                }
                if ($key == 'callemergency'){
                    EmergencyToDo::where('user_id', $authUser->id)
                        ->where('type', 'callemergency')
                        ->update(['sort_id' => $value['sort'], 'active' => $value['active']]);
                }

            }

        }

        return Response::json([
            'message' => 'Success',
            EmergencyToDoResource::collection($authUser->emergencyToDo),
        ], 200);
    }
}
