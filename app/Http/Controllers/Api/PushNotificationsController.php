<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PushNotificationResource;
use App\Models\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class PushNotificationsController extends Controller {

    public function show(Request $request, $userid) {

        $push = PushNotification::where('user_id', $userid)->get();

        return PushNotificationResource::collection($push);

    }

    public function delete(){
        $authUser = Auth::guard('api')->user();

        $push = PushNotification::where('user_id', $authUser->id)->first();

        if ($push){
            $push->delete();
        }

        return Response::json([
            'message' => 'Device ID has been removed.',
        ], 204);
    }

    public function createOrUpdate(Request $request) {

        $validator = Validator::make(
            [
                'device_id' => $request->input('device_id'),
                'os' => $request->input('os'),
                'type' => $request->input('type'),
            ],
            [
                'device_id' => ['required', 'max:255'],
                'os' => ['required', 'max:255', 'in:ios,android'],
                'type' => ['required', 'in:phone,tablet'],
            ],
            [
                'device_id.required' => 'device id required',
                'os.required' => 'os required',
                'os.in' => 'os need ios or android',
                'type.required' => 'type required',
                'type.in' => 'type need phone or tablet',
            ]
        );

        if($validator->fails()) {

            return Response::json([
                'status_code' => 404,
                'message' => $validator->messages(),
            ], 404);

        }else{

            $authUser = Auth::guard('api')->user();

            PushNotification::updateOrCreate(
                [
                    'user_id' => $authUser->id,
                ],
                [
                    'device_id' => $request->input('device_id'),
                    'os' => $request->input('os'),
                    'type' => $request->input('type')
                ]
            );

            return Response::json([
                'message' => 'Success',
            ], 200);
        }
    }
}