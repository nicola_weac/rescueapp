<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ContactResource;
use App\Models\Contacts;
use App\Models\DefaultContacts;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function showContacts(){

        $authUser = Auth::guard('api')->user();
//dd(Auth::guard('api'));
        return ContactResource::collection($authUser->contacts);
    }

    public function addContact(Request $request){
        $v = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->errors(),
            ], 404);
        }else{
            $authUser = Auth::guard('api')->user();

            $recipient = User::where('email', $request->input('email'))->orWhere('name', $request->input('email'))->first();
            if ($authUser->email == $recipient->email){
                return Response::json([
                    'message' => 'You cannot add yourself.'
                ], 404);
            }
            if ($recipient){

                if (Contacts::where('recipient_id', $recipient->id)->where('sender_id', $authUser->id)->first()){

                    return Response::json([
                        'message' => 'User is already in your Contact list.'
                    ], 404);
                }else{
                    Contacts::create([
                        'sender_id' => $authUser->id,
                        'recipient_id' => $recipient->id
                    ]);
                }
            }else{
                return Response::json([
                    'message' => 'User does not exist.'
                ], 404);
            }
            $Contacts = Contacts::where('sender_id', $authUser->id)->count();
            if ($Contacts == 1){
                $this->defaultContact(Contacts::where('sender_id', $authUser->id)->first()->id);
            }
        }

        return Response::json([
            'message' => 'Successfully added.',
        ], 200);
    }

    public function deleteContact($contactid){

        $authUser = Auth::guard('api')->user();
        $Contact = Contacts::where('sender_id', $authUser->id)->where('id', $contactid)->first();

        if (!$Contact){
            return Response::json([
                'message' => 'Contact not found.'
            ], 404);
        }
        $Contact->delete();

        return Response::json([
            'message' => 'Successfully removed User from Contacts.',
        ], 200);
    }

    public function defaultContact($contactid){

        $authUser = Auth::guard('api')->user();

        $Contact = Contacts::findOrFail($contactid);

        DefaultContacts::updateOrCreate(
            ['user_id' => $authUser->id],
            ['default_contact_id' => $Contact->id]
        );

        return Response::json([
            'message' => 'Successfully added to your default contact.'
        ], 200);

    }
}
