<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{
    public function editPassword(Request $request){

        $v = Validator::make($request->all(), [
            'new_password' => 'required|confirmed|min:6',
            'old_password' => 'required|min:6'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();

            if (Hash::check($request->input('old_password'), $authUser->password)){

                $User = User::find($authUser->id);
                $User->password = bcrypt($request->input('new_password'));
                $User->save();

            }else{
                return Response::json([
                    'message' => 'Wrong Password.'
                ], 404);
            }
        }
        return Response::json([
            'message' => 'Password has been updated.'
        ], 200);

    }
}
