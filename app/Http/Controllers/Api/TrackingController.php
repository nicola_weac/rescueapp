<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TrackingResource;
use App\Models\ClientAlive;
use App\Models\Contacts;
use App\Models\ContactTracking;
use App\Models\Position;
use App\Models\TrackingStartedNotification;
use App\Models\PushNotification;
use App\Models\Tracking;
use App\Models\Route;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class TrackingController extends Controller
{
    public function getCurrentTracking(){

        /** @var User $authUser */
        $authUser = Auth::guard('api')->user();

        if ($authUser->activeTracking){

            return new TrackingResource($authUser->activeTracking);
        }

        return Response::json([
            'message' => 'No active Tracking'
        ], 404);

    }

    public function cancelTracking($trackingid){

        $authUser = Auth::guard('api')->user();

        $Tracking = Tracking::find($trackingid);

        $contactid = Contacts::where('sender_id', $Tracking->user_id)->where('recipient_id', $authUser->id)->pluck('id');
        $Tracking->trackingContact()->where('contact_id', $contactid)->where('tracking_id', $trackingid)->update(['cancelled' => true]);

        $pushnotification = PushNotification::where('user_id', $Tracking->user_id)->first();

        if ($pushnotification){
            $push = new \Edujugon\PushNotification\PushNotification('apn');
            $push->setMessage([
                'aps' => [
                    'alert' => [
                        'title' => 'Guarding abgebrochen',
                        'body' =>  $authUser->profile->firstname . ' hat aufgehört dich zu tracken.'
                    ],
                    'sound' => 'default',
                    'badge' => 1,
                ],
            ])
                ->setDevicesToken($pushnotification->device_id)
                ->send();

            $notification = TrackingStartedNotification::where('tracking_id', $trackingid)->get();

            if ($notification){
                foreach ($notification as $value){
                    $value->update([
                        'active' => false
                    ]);
                }
            }

        }

        return Response::json([
            'message' => 'success',
        ], 200);
    }

    public function deny($trackingid){

        $authUser = Auth::guard('api')->user();

        /** @var Tracking $Tracking */
        $Tracking = Tracking::find($trackingid);

        if (!$Tracking){
            return Response::json([
                'message' => 'Tracking not found',
            ], 404);
        }

        $notification = TrackingStartedNotification::where('tracking_id', $trackingid)
            ->where('requested_user_id', $authUser->id)->first();

        if($notification){
            $notification->update(['active' => false]);
        }

        //$Trackings = Tracking::where('delphos', 0)->where('emergency', 0)->where('status', 0)->where('friendinfo', 0)->get();
        $toDo = $Tracking->trackingContact()->count();

        $contact = Contacts::where('sender_id', $Tracking->user_id)->where('recipient_id', $authUser->id)->first();
        $Tracking->trackingContact()->where('contact_id', $contact->id)->where('tracking_id', $trackingid)->update(['cancelled' => true]);

        $pushnotification = PushNotification::where('user_id', $Tracking->user_id)->first();

        if ($pushnotification){
            if($toDo == 1){
                $Tracking->friendinfo = 1;
                $Tracking->update();

                $push = new \Edujugon\PushNotification\PushNotification('apn');
                $push->setMessage([
                    'aps' => [
                        'alert' => [
                            'title' => 'Guarding abgelehnt',
                            'body' =>  $authUser->profile->firstname . ' kann für den Zeitraum leider nicht dein Guard sein.'
                        ],
                        'sound' => 'default',
                        'badge' => 1,
                        'category' => 'delphos.noguards'
                    ],
                ])
                    ->setDevicesToken($pushnotification->device_id)
                    ->send();
            }else{
                $push = new \Edujugon\PushNotification\PushNotification('apn');
                $push->setMessage([
                    'aps' => [
                        'alert' => [
                            'title' => 'Guarding abgelehnt',
                            'body' =>  $authUser->profile->firstname . ' kann für den Zeitraum leider nicht dein Guard sein.'
                        ],
                        'sound' => 'default',
                        'badge' => 1
                    ],
                ])
                    ->setDevicesToken($pushnotification->device_id)
                    ->send();
            }
        }
        return Response::json([
            'message' => 'Success'
        ], 200);
    }

    public function accept($trackingid){

        $authUser = Auth::guard('api')->user();

        /** @var Tracking $Tracking */
        $Tracking = Tracking::find($trackingid);
        if (!$Tracking){
            return Response::json([
                'message' => 'Tracking not found',
            ], 404);
        }

        $notification = TrackingStartedNotification::where('tracking_id', $trackingid)
            ->where('requested_user_id', $authUser->id)->first();

        if($notification){
            $notification->update(['active' => false]);
        }

        /** @var Contacts $contact */
        $contact = Contacts::where('sender_id', $Tracking->user_id)->where('recipient_id', $authUser->id)->first();
        $Tracking->trackingContact()->where('contact_id', $contact->id)->where('tracking_id', $trackingid)->update(['accepted' => true]);

        if($Tracking->status = 'inactive'){

            $Tracking->update(['update_countdown' => Carbon::now()->addMinutes($Tracking->notify_period)->addSeconds(20)->format('Y-m-d H:i:s')]);

        }

        $Tracking->update(['status' => 'active']);

        $pushnotification = PushNotification::where('user_id', $Tracking->user_id)->first();

        if ($pushnotification){
            $push = new \Edujugon\PushNotification\PushNotification('apn');
            $push->setMessage([
                'aps' => [
                    'alert' => [
                        'title' => 'Guarding angenommen',
                        'body' =>  $authUser->profile->firstname . ' hat deine Anfrage als Guard angenommen.'
                    ],
                    'sound' => 'default',
                    'badge' => 1,
                    'category' => 'delphos.guard.start'
                ],
            ])
                ->setDevicesToken($pushnotification->device_id)
                ->send();
        }

        return Response::json([
            'message' => 'Success',
        ], 200);
    }

    public function alive()
    {
        $authUser = Auth::guard('api')->user();

        ClientAlive::create([
            'user_id' => $authUser->id,
            'tracking_id' => $authUser->activeTracking->id,
        ]);

        Tracking::find($authUser->activeTracking->id)
            ->update(['update_countdown' => Carbon::now()->addMinutes(1)->addSeconds(20)->format('Y-m-d H:i:s')]);

        return Response::json([
            'message' => 'Success',
        ], 200);
    }

    public function showHistory()
    {
        $authUser = Auth::guard('api')->user();
        if ($authUser->contacts){
            $contacts = $authUser->iscontact()->pluck('id');
            $trackingids = ContactTracking::whereIn('contact_id', $contacts)->pluck('tracking_id');
            $trackings = Tracking::whereIn('id', $trackingids)->orderBy('created_at', 'DESC')->get();
        }

        $Trackings = $authUser->trackings;

        if ($trackings){
            $concatenated = $Trackings->concat($trackings);
        }

        return TrackingResource::collection($concatenated->sortByDesc('id'));
    }

    public function startTracking(Request $request)
    {
        $v = Validator::make($request->all(), [
            'notify_period' => 'required|numeric',
            'from_location.lat' => '',
            'from_location.lng' => '',
            'to_location.lat' => '',
            'to_location.lng' => '',
            'delphos' => 'boolean',
            'contacts' => 'array'
        ]);

        if ($v->fails()) {
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        } else {

            $authUser = Auth::guard('api')->user();
            $exist = Tracking::where('user_id', $authUser->id)->where('status', "inaktiv")->first();

            if ($exist){
               return Response::json([
                   'message' => 'Already tracking.',
               ], 404);
           }

            $Tracking = new Tracking();
            $Tracking->user_id = $authUser->id;
            $Tracking->notify_period = $request->input('notify_period');

            $Tracking->status = 'inactive';

            if ($request->input('delphos') == 1) {
                $Tracking->delphos = 1;
                $Tracking->status = 'active';
            } else {
                $Tracking->delphos = 0;
            }
            if($Tracking->delphos == 1){
                $Tracking->update_countdown = Carbon::now()
                    ->addMinutes($request->input('notify_period'))
                    ->addSeconds(20)
                    ->format('Y-m-d H:i:s');
            }else{
                $Tracking->update_countdown = Carbon::now()
                    ->addMinutes($request->input('notify_period'))
                    ->addMinutes(3)
                    ->addSeconds(20)
                    ->format('Y-m-d H:i:s');
            }

            $Tracking->save();

            $helper = [];
            if ($request->has('contacts')) {
                if (is_array($request->input('contacts'))) {

                    foreach ($request->input('contacts') as $key => $value) {
                        $helper[] = $value;
                        $Tracking->trackingContact()->create([
                            'contact_id' => $value,
                            'status' => 0,
                        ]);
                    }
                }
            }

            $contacts = Contacts::whereIn('id', $helper)->get();
            $plucked = $contacts->pluck('recipient_id');

            foreach ($plucked as $key => $value){
                TrackingStartedNotification::create([
                    'user_id' => $authUser->id,
                    'tracking_id' => $Tracking->id,
                    'requested_user_id' => $value,
                    'active' => true
                ]);
            }

            $pushnotification = PushNotification::whereIn('user_id', $plucked)->get();
            if ($pushnotification){
                foreach ($pushnotification as $key => $value){
                    $push = new \Edujugon\PushNotification\PushNotification('apn');
                    $push->setMessage([
                        'aps' => [
                            'alert' => [
                                'title' => 'Guarding Anfrage',
                                'body' =>  $authUser->profile->firstname . ' fragt ob du sein Guard sein kannst.'
                            ],
                            'sound' => 'default',
                            'badge' => 1,
                            'category' => 'tracking.request'
                        ],
                        'custom' => [
                            'ids' => [
                                $Tracking->id,
                            ],
                            'media' => $authUser->media()->get()->last() == null ? null : $authUser->media()->get()->last()->url,
                            'name' => $authUser->name
                        ],
                    ])
                        ->setDevicesToken($value->device_id)
                        ->send();

                }
            }

        }

        if($request->has('location_from')){
            Position::create([
                'user_id' => $authUser->id,
                'tracking_id' => $Tracking->id,
                'lat' => $request->input('location_from.lat'),
                'lng' => $request->input('location_from.lng'),
                'city' => $request->input('location_from.city'),
                'zip' => $request->input('location_from.zip'),
                'street' => $request->input('location_from.street'),
            ]);
        }

        if ($request->has('location_to') && $request->has('location_from')) {

            Route::create([
                'tracking_id' => $Tracking->id,
                'from_lat' => $request->input('location_from.lat'),
                'from_lng' => $request->input('location_from.lng'),
                'from_city' => $request->input('location_from.city'),
                'from_zip' => $request->input('location_from.zip'),
                'from_street' => $request->input('location_from.street'),

                'to_lat' => $request->input('location_to.lat'),
                'to_lng' => $request->input('location_to.lng'),
                'to_city' => $request->input('location_to.city'),
                'to_zip' => $request->input('location_to.zip'),
                'to_street' => $request->input('location_to.street'),
            ]);
        }

        return Response::json([
            'message' => 'Tracking has been started.',
            'data' => new TrackingResource($Tracking)
        ], 200);
    }

    public function stopTracking(){

        $authUser = Auth::guard('api')->user();
        /** @todo: find & Update siehe andere beispiele von mir */
        $Tracking = Tracking::where('user_id', $authUser->id)->where('status', '!=', 'finished')->first();
        if ($Tracking) {
            $Tracking->status = 'finished';
            $Tracking->save();
            $ContactTracking = ContactTracking::where('tracking_id', $Tracking->id)->where("accepted",1)->get();
            if ($ContactTracking->count() > 0) {
                $helper = [];
                foreach ($ContactTracking as $key => $value){
                    $helper[] = $value->contact_id;
                    $value->status = 1;
                    $value->save();
                }

                $contacts = Contacts::whereIn('id', $helper)->select('recipient_id')->get();

                $pushnotification = PushNotification::whereIn('user_id', $contacts)->get();
                if ($pushnotification){
                    foreach ($pushnotification as $key => $value){
                        $push = new \Edujugon\PushNotification\PushNotification('apn');
                        $push->setMessage([
                            'aps' => [
                                'alert' => [
                                    'title' => 'Guarding Ende',
                                    'body' => User::find($Tracking->user_id)->profile->firstname .' ist sicher am Ziel angekommen. '.User::find($Tracking->user_id)->profile->firstname . ' bedankt sich für dein Guarding.'
                                ],
                                'sound' => 'default',
                                'badge' => 1
                            ],
                            'custom' => [

                            ],
                        ])
                            ->setDevicesToken($value->device_id)
                            ->send();
                    }
                }
            }
            /** @var TrackingStartedNotification $notification */
            $notification = TrackingStartedNotification::where('tracking_id', $Tracking->id)->get();

            if ($notification){
                foreach ($notification as $value){
                    $value->update(['active' => false]);
                }
            }
        }

        return Response::json([
            'message' => 'Tracking has been stopped.'
        ], 200);
    }
}