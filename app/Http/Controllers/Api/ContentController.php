<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ContentResource;
use App\Models\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ContentController extends Controller
{
    public function index($identifier){

        $Content = Content::where('identifier', $identifier);

        if($Content->count() > 0){
            $Content = $Content->first();

        }else{
            return Response::json([
                'message' => 'Not found',
            ], 404);
        }

        return Response::json([
            'message' => 'Content.',
            'data'  => [new ContentResource($Content)]
        ]);
    }
}
