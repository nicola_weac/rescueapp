<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Models\Emergency;
use App\Models\PushNotification;
use App\Models\Route;
use App\Models\Tracking;
use App\Models\UserProfile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class UserController extends Controller
{
    public function deleteAccount(){
        $authUser = Auth::guard('api')->user();

        $authUser->contacts()->delete();
        $authUser->iscontact()->delete();
        $authUser->positions()->delete();
        foreach ($authUser->trackings as $key => $value){
            $value->route()->delete();
        }
        $authUser->trackings()->delete();
        foreach ($authUser->emergencies() as $key => $value){
            $value->files()->delete();
        }
        $authUser->emergencies()->delete();
        $authUser->clientalive()->delete();
        $authUser->delphosLicense()->delete();
        $authUser->profile()->delete();

        $authUser->name = 'anonymus';
        $authUser->email = 'anonymus';
        $authUser->save();
        $authUser->delete();

        return Response::json([
            'message' => 'Success.'
        ], 200);
    }

    public function repeat_notifications(Request $request){
        $authUser = Auth::guard('api')->user();

        if(!empty($request->input('tracking'))){

            $Trackings = Tracking::where('user_id', $authUser->id)->where('status', 0);

            if($Trackings->count() > 0){

                $value = $Trackings->first();
                if($value->emergency == 1){

                    if (Carbon::now()->format('Y-m-d H:i:s') >= $value->update_countdown){

                        if (Emergency::where('tracking_id', $value->id)->count() == 1){

                            $pushown = PushNotification::where('user_id', $value->user_id)->first();
                            if ($pushown) {

                                $push = new \Edujugon\PushNotification\PushNotification('apn');
                                $push->setMessage([
                                    'aps' => [
                                        'alert' => [
                                            'title' => 'Automatische Notfallauslösung',
                                            'body' => 'Deine ausgewählten Kontakte wurde über dein Notfall informiert'
                                        ],
                                        'sound' => 'default',
                                        'badge' => 1,
                                        'category' => 'delphos.alarm'
                                    ],
                                ])->setDevicesToken($pushown->device_id)->send();;
                            }

                        }
                    }
                }
            }
        }

    }

    public function passwordEdit(Request $request){

        $v = Validator::make($request->all(), [
            'new_password' => 'required|string|min:6|confirmed',
            'old_password' => 'required|string|min:6'
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->messages()
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();

            if (Hash::check($request->input('old_password'), $authUser->password)){

                $User = User::find($authUser->id);
                $User->password = bcrypt($request->input('new_password'));
                $User->save();
            }else{
                return Response::json([
                    'message' => 'Password wrong.'
                ], 404);
            }

        }

        return Response::json([
            'message' => 'Password has been updated.'
        ],200);
    }

    public function updateMedia(Request $request){

        $v = Validator::make($request->all(), [
            'file' => 'required',
            'image_group' => 'string'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages()
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();

            if($request->hasFile('file')) {
                if ($request->file('file') >= 1) {
                    $authUser->saveMedia($request->file('file'), $request->input('image_group'), 'single');
                }
            }

        }
        return Response::json([
            'message' => 'Successfully updated',
        ], 200);
    }

    public function updateProfile(Request $request){

        $v = Validator::make($request->all(), [
            'firstname' => '',
            'lastname' => '',
            'street' => '',
            'zip' => '',
            'city' => '',
            'phone' => '',
            'country' => '',
            'age' => '',
            'gender' => '',
            'height' => '',
            'intolerances' => '',
            'pre_existing_conditions' => '',
            'medication' => '',
            'blood_type' => ''
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();

            $UserProfile = UserProfile::where('user_id', $authUser->id)->first();

            $UserProfile->firstname = $request->input('firstname');
            $UserProfile->lastname = $request->input('lastname');
            $UserProfile->street = $request->input('street');
            $UserProfile->zip = $request->input('zip');
            $UserProfile->city = $request->input('city');
            $UserProfile->phone = $request->input('phone');
            $UserProfile->country = $request->input('country');
            $UserProfile->age = $request->input('age');
            $UserProfile->gender = $request->input('gender');
            $UserProfile->height = $request->input('height');
            $UserProfile->intolerances = $request->input('intolerances');
            $UserProfile->pre_existing_conditions = $request->input('pre_existing_conditions');
            $UserProfile->medication = $request->input('medication');
            $UserProfile->blood_type = $request->input('blood_type');

            $UserProfile->save();
        }

        return Response::json([
            'message' => 'Profile has been updated.',
            'data' => new UserResource($authUser)
        ], 200);

    }

    public function showProfile(Request $request){

        $authUser = Auth::guard('api')->user();

        return new UserResource($authUser);
    }
}
