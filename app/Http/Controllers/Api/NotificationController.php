<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TrackingStartedNotificationResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function showNotificationsOnStart()
    {
        /** @var User $authUser */
        $authUser = auth('api')->user();

        $notifications = $authUser->trackingStartedNotifications()->orderBy('created_at', 'DESC')->get();

        return TrackingStartedNotificationResource::collection($notifications);
    }
}
