<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\EmergencyResource;
use App\Models\ContactEmergency;
use App\Models\Contacts;
use App\Models\ContactTracking;
use App\Models\DefaultContacts;
use App\Models\Emergency;
use App\Models\Position;
use App\Models\Tracking;
use App\Models\PushNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class EmergencyController extends Controller
{
    public $onlyme = false;

    public function sendAutoEmergency($trackingid){

        $Tracking = Tracking::find($trackingid);
        $Tracking->emergency = 1;
        $Tracking->save();


        $pushown = PushNotification::where('user_id', $Tracking->user_id)->first();
        if ($pushown){
            $push = new \Edujugon\PushNotification\PushNotification('apn');
            $push->setMessage([
                'aps' => [
                    'alert' => [
                        'title' => 'Automatische Notfallauslösung',
                        'body' =>   'Deine ausgewählten Kontakte wurde über dein Notfall informiert'
                    ],
                    'sound' => 'default',
                    'badge' => 1,
                    'category' => 'delphos.alarm'
                ],
            ])
                ->setDevicesToken($pushown->device_id)
                ->send();
        }

        $User = User::find($Tracking->user_id);

        $Emergency = Emergency::create([
            'user_id' => $User->id,
            'tracking_id' => $trackingid,
            'delphos' => $Tracking->delphos == 0 ? false : true,
            'police' => null,
            'ambulance' => null,
            'lat' => $Tracking->position->last()->lat,
            'lng' => $Tracking->position->last()->lng,
        ]);

        $ContactTracking = ContactTracking::where('tracking_id', $trackingid)->select('contact_id')->get();
        foreach ($ContactTracking as $key => $value){
            ContactEmergency::create([
                'emergency_id' => $Emergency->id,
                'contact_id' => $value->contact_id
            ]);
        }


        $Contacts = Contacts::whereIn('id', $ContactTracking)->select('recipient_id')->get();
        $pushnotification = PushNotification::whereIn('user_id', $Contacts)->get();

        if($pushnotification->count() > 0 && $this->onlyme == false){
            foreach ($pushnotification as $key => $value){
                $push = new \Edujugon\PushNotification\PushNotification('apn');
                $push->setMessage([
                    'aps' => [
                        'alert' => [
                            'title' => 'Automatische Notfallauslösung',
                            'body' => $User->profile->firstname . ' reagiert nicht mehr. Bitte kümmere dich um '.$User->profile->firstname
                        ],
                        'sound' => 'default',
                        'badge' => 1,
                        'category' => 'delphos.guard.alarm'
                    ],
                    'custom' => [
                        'id' => (int)$trackingid,
                    ],
                ])
                    ->setDevicesToken($value->device_id)
                    ->send();
            }
        }



    }

    public function sendEmergency(Request $request){
    
        $v = Validator::make($request->all(), [
            'police' => 'required',
            'ambulance' => 'required',
            'guardHelp' => 'required',
            'location.lat' => '',
            'location.lng' => '',
            'contacts' => '',
            'delphos' => ''
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();
            $Tracking = Tracking::where('user_id', $authUser->id)->where('status', 'active')
                ->update(['emergency' => true]);

            /*
            $exist = Emergency::where('user_id', $authUser->id)->where('status', 0)->first();

            if($exist){
                return Response::json([
                    'message' => 'Active Emergency.'
                ]);
            }
            */

            $Emergency = new Emergency();
            $Emergency->user_id = $authUser->id;
            $Emergency->police = $request->input('police');
            $Emergency->ambulance = $request->input('ambulance');
            $Emergency->guardHelp = $request->input('guardHelp');
            $Emergency->lat = $request->input('location.lat');
            $Emergency->lng = $request->input('location.lng');
            if ($request->has('delphos')) {
                $Emergency->delphos = $request->input('delphos');
            }else{
                $Emergency->delphos = $Tracking == false ? 0 : $Tracking->delphos;
            }
            $Tracking = Tracking::where('user_id', $authUser->id)->where('status', 'active')->first();
            if(!$Tracking){
                $Tracking = new Tracking();
                $Tracking->user_id = $authUser->id;
                $Tracking->notify_period = 1;
                $Tracking->status = 'active';
                if ($request->input('delphos') == 1) {
                    $Tracking->delphos = 1;
                } else {
                    $Tracking->delphos = 0;
                }
                $Tracking->update_countdown = Carbon::now()->addMinutes($request->input('notify_period'))->addSeconds(20)->format('Y-m-d H:i:s');
                $Tracking->emergency = 1;
                $Tracking->save();

            }

            Position::create([
                'user_id' => $authUser->id,
                'tracking_id' => $Tracking->id,
                'lat' => $request->input('location.lat'),
                'lng' => $request->input('location.lng'),
                'street' => $request->input('location.street'),
                'zip' => $request->input('location.zip'),
                'city' => $request->input('location.city'),
            ]);


            $Emergency->tracking_id = $Tracking->id;
            $Emergency->save();

            if ($request->has('contacts')) {

                $helper = [];
                if (is_array($request->input('contacts'))) {

                    foreach ($request->input('contacts') as $key => $value) {
                        $helper[] = $value;
                        ContactEmergency::create([
                            'emergency_id' => $Emergency->id,
                            'contact_id' => $value,
                        ]);
                        ContactTracking::create([
                            'tracking_id' => $Tracking->id,
                            'contact_id' => $value,
                        ]);
                    }
                }
                $contacts = Contacts::whereIn('id', $helper)->select('recipient_id')->get();
                $in = [];
                if ($contacts->count() > 0) {
                    foreach ($contacts as $key => $value) {
                        $in[] = $value->recipient_id;
                    }
                }
                $pushnotification = PushNotification::whereIn('user_id', $in)->get();

                if ($pushnotification->count() > 0) {

                    foreach ($pushnotification as $key => $value) {
                        $push = new \Edujugon\PushNotification\PushNotification('apn');
                        $push->setMessage([
                            'aps' => [
                                'alert' => [
                                    'title' => 'Notfall ausgelöst',
                                    'body' => $authUser->profile->firstname . ' hat einen Notfall ausgelöst.'
                                ],
                                'sound' => 'default',
                                'badge' => 1,
                                'category' => 'delphos.guard.alarm'
                            ],
                            'custom' => [
                                'id' => (int)$Tracking->id,
                            ],
                        ])
                            ->setDevicesToken($value->device_id)
                            ->send();
                    }
                }
            }else{
                $Default = DefaultContacts::where("user_id",$authUser->id)->first();

                ContactEmergency::create([
                    'emergency_id' => $Emergency->id,
                    'contact_id' => $Default->default_contact_id,
                ]);
                ContactTracking::create([
                    'tracking_id' => $Tracking->id,
                    'contact_id' => $Default->default_contact_id,
                ]);

                $pushnotification = PushNotification::whereIn('user_id', [$Default->defaultUser->recipient_id])->get();

                if ($pushnotification->count() > 0) {

                    foreach ($pushnotification as $key => $value) {
                        $push = new \Edujugon\PushNotification\PushNotification('apn');
                        $push->setMessage([
                            'aps' => [
                                'alert' => [
                                    'title' => 'Notfall ausgelöst',
                                    'body' => $authUser->profile->firstname . ' hat einen Notfall ausgelöst.'
                                ],
                                'sound' => 'default',
                                'badge' => 1,
                                'category' => 'delphos.guard.alarm'
                            ],
                            'custom' => [
                                'id' => (int)$Tracking->id,
                            ],
                        ])
                            ->setDevicesToken($value->device_id)
                            ->send();
                    }
                }
            }
        }

        return Response::json([
            'message' => 'Emergency has been called.',
        ], 200);
    }

}
