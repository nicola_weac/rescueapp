<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Mail\PasswordResetCode;
use App\Models\PasswordResetCodes;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{

    public function reset(Request $request){

        $v = Validator::make($request->all(), [
            'pin' => 'min:6|max:6',
            'new_password' => 'required|confirmed|string|min:6'
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->messages()
            ], 404);
        }else{

            $email = PasswordResetCodes::where('code', $request->input('pin'))
                ->where('expiry_date', '>', date('Y-m-d H:i:s'))
                ->first()->email;

            $PasswordResetCode = PasswordResetCodes::where('email', $email)
                ->where('expiry_date', '>', date('Y-m-d H:i:s'))
                ->orderBy('expiry_date', 'DESC')
                ->first();

            if($request->input('pin') == $PasswordResetCode->code){

                $User = User::where('email', $email)->first();

                $User->password = bcrypt($request->input('new_password'));

                $User->save();

            }else{
                return Response::json([
                    'message' => 'Pin is wrong.',
                ], 404);
            }
        }

        PasswordResetCodes::where('code', $request->input('pin'))->delete();

        return Response::json([
            'message' => 'Password has been updated.'
        ],200);
    }

    public function sendResetCode(Request $request){
        $v = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->messages()
            ], 404);
        }else{

            if($this->existsUser($request->input('email')) == true){

                $code = $this->generator();
                $PasswordResetCodes = PasswordResetCodes::where('expiry_date', '>', date('Y-m-d H:i:s'))->get();

                $arr = [];
                foreach ($PasswordResetCodes as $key => $value){
                    $arr[] = $value->code;
                }

                while (array_has($arr, $code)){
                    $code = $this->generator();
                }

                $User = User::where('email', $request->input('email'))->first();

                PasswordResetCodes::create([
                    'user_id' => $User->id,
                    'email' => $User->email,
                    'code' => $code,
                    'expiry_date' => Carbon::now()->addHour(1),
                ]);

                Mail::to($request->input('email'))->send(new PasswordResetCode($code));

            }else{
                return Response::json([
                    'message' => 'User not found.',
                ], 404);
            }


        }
        return Response::json([
            'message' => 'Email has been sent.'
         ], 200);
    }

    protected function existsUser($email){

        $User = User::where('email', $email)->first();

        if($User != null){
            return true;
        }else{
            return false;
        }
    }

    protected function generator(){

        $str = '1234567890';
        $ret = substr(str_shuffle($str), 0, 6);

        return $ret;
    }
}