<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AuthHelpController extends Controller
{
    public function usernameCheck(Request $request){

        $username = User::where('name', $request->input('username'))->first();

        if ($username){
            return Response::json([
                'message' => 'Username is taken.'
            ], 404);
        }

        return Response::json([
            'message' => 'Username is available',
        ], 200);
    }

    public function emailCheck(Request $request){

        $v = Validator::make($request->all(), [
            'email' => 'email'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{
            $email = User::where('email', $request->input('email'))->first();

            if ($email){
                return Response::json([
                    'message' => 'Email is taken.'
                ], 404);
            }
        }

        return Response::json([
            'message' => 'Email is available',
        ], 200);
    }
}
