<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Resources\MediaResource;
use App\Mail\RegisterConfirmation;
use App\Models\DelphosLicense;
use App\Models\EmergencyToDo;
use App\Models\UserProfile;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class AuthController extends Controller
{

    private $client;

    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    public function login(Request $request){

        $validator = [
            'email'         =>      'required|email',
            'password'      =>      'required|string|min:6'
        ];

        $v = Validator::make(Input::all(), $validator);

        if($v->fails()){
            return Response::json([
                'status_code' => 404,
                'messages'    => $v->messages(),
            ],404);
        }else{
            $request->request->add([
                'email' => $request->email,
                'username'  =>  $request->email,
                'password' => $request->password,
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => '*'
            ]);

            $proxy = Request::create(
                'api/oauth/token',
                'POST'
            );

            $Response = Route::dispatch($proxy);

            if($Response->getStatusCode() != 200) {
                $jsonData = \json_decode($Response->getContent(), true);

                return Response::json([
                    'status_code' => $Response->getStatusCode(),
                    'message' => "Wir konnten deine eingetragenen Zugangsdaten nicht im System finden.",
                    'email' => $request->email,
                ], $Response->getStatusCode());
            }

            $User = User::where('email', $request->input('email'))->first();
            $UserProfile = UserProfile::where('user_id', $User->id)->first();
            $jsonData = \json_decode($Response->getContent(), true);
            $arr = [
                'id' => $User->id,
                'email' => $request->input('email'),
                'firstname' => $UserProfile->firstname,
                'lastname' => $UserProfile->lastname,
                'media' => $User->media()->first() != null ? new MediaResource($User->media()->first()) : null,
            ];
            $data = array_merge($jsonData, $arr);
        }

        return Response::json($data, $Response->getStatusCode());
    }

    public function register(Request $request){
        $validator = [
            'name'      =>      'required|min:3|max:20|string|unique:users,name',
            'email'     =>      'required|email|unique:users,email',
            'password'  =>      'required|min:6|max:255|string',
        ];

        $v = Validator::make(Input::all(), $validator);

        if($v->fails()){

            return Response::json([
                'message' => $v->errors(),
            ], 404);

        }else{

            $User = User::create([
                'name'          =>          $request->input('name'),
                'email'         =>          $request->input('email'),
                'password'      =>          bcrypt($request->input('password')),
            ]);

            $UserProfile = UserProfile::create([
                'user_id' => $User->id,
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'street' => null,
                'zip' => null,
                'city' => null,
                'phone' => $request->input('phone')
            ]);

            DelphosLicense::create([
                'user_id' => $User->id,
                'hasLicense' => 0,
                'expiry_date' => null,
            ]);

            $todos = [
                [
                    'user_id' => $User->id,
                    'type' => 'callme',
                    'name' => 'Mich anrufen',
                    'sort_id' => 1,
                    'active' => true
                ], [
                    'user_id' => $User->id,
                    'type' => 'callcontact',
                    'name' => 'Notfallkontakt anrufen',
                    'sort_id' => 2,
                    'active' => true
                ], [
                    'user_id' => $User->id,
                    'type' => 'callemergency',
                    'name' => 'Notdienst anrufen',
                    'sort_id' => 3,
                    'active' => true
                ]
            ];

            foreach ($todos as $value){

                EmergencyToDo::create($value);

            }


            $request->request->add([
                'username' => $request->email,
                'email'     => $request->email,
                'password' => $request->password,
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => '*'
            ]);

            $proxy = Request::create(
                'api/oauth/token',
                'POST'
            );

            $Response = Route::dispatch($proxy);
            $jsonData = \json_decode($Response->getContent(), true);
            $arr = [
                'id' => $User->id,
                'email' => $request->input('email'),
                'firstname' => $UserProfile->firstname,
                'lastname' => $UserProfile->lastname,
            ];
            $data = array_merge($jsonData, $arr);

            Mail::to($User->email)->send(new RegisterConfirmation($User));

            return Response::json($data, $Response->getStatusCode());
        }
    }
}
