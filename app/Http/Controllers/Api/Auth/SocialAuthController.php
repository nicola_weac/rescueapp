<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegisterConfirmation;
use App\Models\DelphosLicense;
use App\Models\EmergencyToDo;
use App\Models\FacebookUsers;
use App\Models\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class SocialAuthController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    public function createOrGetUser(Request $request){

        $v = Validator::make($request->all(), [
            'email' => 'email',
            'firstname' => 'required',
            'lastname' => 'required',
            'userID' => 'required',
            'pictureURL' => ''
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $facebookUser = FacebookUsers::where('provider_user_id', $request->input('userID'))->first();

            $password = $request->input('userID') . 'supercalifragilisticexpialidocious';

            if($facebookUser == null) {
                $User = User::create([
                    'name' => strtolower($request->input('firstname') . $request->input('lastname')),
                    'email' => $request->input('email'),
                    'password' => Hash::make($password),
                ]);

                UserProfile::create([
                    'user_id' => $User->id,
                    'firstname' => $request->input('firstname'),
                    'lastname' => $request->input('lastname'),
                ]);

                $facebookUser = FacebookUsers::create([
                    'user_id' => $User->id,
                    'provider_user_id' => $request->input('userID'),
                    'provider' => 'facebook'
                ]);

                DelphosLicense::create([
                    'user_id' => $User->id,
                    'hasLicense' => false,
                    'expiry_date' => null
                ]);

                EmergencyToDo::create([
                    'user_id' => $User->id,
                    'type' => 'callme',
                    'name' => 'Mich anrufen',
                    'sort_id' => 1,
                    'active' => true
                ]);

                EmergencyToDo::create([
                    'user_id' => $User->id,
                    'type' => 'callcontact',
                    'name' => 'Notfallkontakt anrufen',
                    'sort_id' => 2,
                    'active' => true
                ]);

                EmergencyToDo::create([
                    'user_id' => $User->id,
                    'type' => 'callemergency',
                    'name' => 'Notdienst anrufen',
                    'sort_id' => 3,
                    'active' => true
                ]);

                Mail::to($User->email)->send(new RegisterConfirmation($User));

            }

            $User = User::find($facebookUser->user_id);

            $request->request->add([
                'username' => $User->email,
                'email'     => $User->email,
                'password' => $password,
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => '*'
            ]);

            $proxy = Request::create(
                'api/oauth/token',
                'POST'
            );

            $Response = Route::dispatch($proxy);
            $jsonData = \json_decode($Response->getContent(), true);
            $arr = [
                'email' => $request->input('email'),
            ];
            $data = array_merge($jsonData, $arr);

            return Response::json($data, $Response->getStatusCode());

        }
    }
}
