<?php

namespace App\Http\Controllers\Api;

use App\Mail\LicenseInfos;
use App\Models\DelphosLicense;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class DelphosLicenseController extends Controller
{
    public function sendInfoEmail(){

        $authUser = Auth::guard('api')->user();

        Mail::to($authUser->email)->send(new LicenseInfos($authUser));

        return Response::json([
            'message' => 'Email has been sent.'
        ], 200);
    }

    public function hasLicense(){

        $authUser = Auth::guard('api')->user();
        $license = DelphosLicense::where('user_id', $authUser->id)->where('hasLicense', 1)->first();
        $this->checkExpiry($license);
        if ($license->hasLicense == 1){
            return Response::json([
                'message' => 'You got a license',
            ], 200);
        }else{
            return Response::json([
                'message' => 'No license bra.',
            ], 404);
        }

        /** so wäre es richtig

        DelphosLicense::where('user_id', $authUser->id)
            ->where('hasLicense', 1)
            ->where('expiry_date', "<" , Carbon::now()->format('Y-m-d'))
            ->update(['hasLicense' => 0]);

        */
    }

    private function checkExpiry($license){

        if ($license->expiry_date < Carbon::now()->format('Y-m-d')){
            $license->hasLicense = 0;
            $license->save();
        }

    }
}
