<?php

namespace App\Http\Controllers\Api;

use App\Models\Emergency;
use App\Models\EmergencyFile;
use App\Models\TrackingPhoneBattery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class EmergencyFileController extends Controller
{
    public function sendEmergencyFiles(Request $request){
        $v = Validator::make($request->all(), [
            'file' => '',
            //'battery_lvl' => '',
            //'battery_state' => '',
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();
            $Emergency = Emergency::where('user_id', $authUser->id)->where('status', 0)->get()->last();

            $File = EmergencyFile::create([
                'emergency_id' => $Emergency->id,
                'user_id' => $authUser->id,
                'path' => $request->file('file')->storeAs('/emergency/' . $Emergency->id, $request->file('file')->getClientOriginalName()),
                'mime' => $request->file('file')->getMimeType(),
                'size' => $request->file('file')->getSize(),
            ]);

            /*$Battery = TrackingPhoneBattery::create([
                'tracking_id' => $Emergency->tracking_id,
                'battery_lvl' => $request->input('batteryLevel'),
                'battery_state' => $this->batteryStateToString($request->input('batteryState')),
            ]);*/


        }
        return Response::json([
            'message' => 'Upload Succesful',
        ], 200);
    }

    private function batteryStateToString($batteryState){

        switch ($batteryState){
            case 0:
                return 'unknown';
                break;
            case 1:
                return 'unplugged';
                break;
            case 2:
                return 'charging';
                break;
            case 3:
                return 'full';
                break;
        }
        return '';
    }
}
