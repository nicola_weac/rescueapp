<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TrackingResource;
use App\Models\ContactEmergency;
use App\Models\Contacts;
use App\Models\ContactTracking;
use App\Models\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Position;
use Illuminate\Support\Facades\Response;

class PositionController extends Controller
{
    public function showPositions(){

        $authUser = Auth::guard('api')->user();
        if(Contacts::where('recipient_id', $authUser->id)->count() > 0){
            $contacts = Contacts::where('recipient_id', $authUser->id)->select('id')->get();
            $tracking = ContactTracking::whereIn('contact_id', $contacts)->where('status', 0)->where('cancelled', 0)->select('tracking_id')->get();

            $Trackings = Tracking::whereIn('id', $tracking)->where('status', 'active')->get();

            return TrackingResource::collection($Trackings);
        }
    }

    public function updatePosition(Request $request){

        $v = Validator::make($request->all(), [
            'location.lat' => 'required',
            'location.lng' => 'required',
            'location.street' => '',
            'location.zip' => '',
            'location.city' => '',
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();
            if($authUser){
                if(Tracking::where('user_id', $authUser->id)->where('status', 'active')->count() > 0){
                    $Tracking = Tracking::where('user_id', $authUser->id)->where('status', 'active')->orderByDesc("id")->first();

                    Position::create([
                        'user_id' => $authUser->id,
                        'tracking_id' => $Tracking->id,
                        'lat' => $request->input('location.lat'),
                        'lng' => $request->input('location.lng'),
                        'street' => $request->input('location.street'),
                        'zip' => $request->input('location.zip'),
                        'city' => $request->input('location.city'),
                    ]);
                }
            }
        }

        return Response::json([
            'message' => 'Position has been updated.'
        ], 200);
    }
}
