<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmergencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'police' => boolval($this->police),
            'ambulance' => boolval($this->ambulance),
            'guardHelp' => boolval($this->guardHelp),
            'lat' => $this->lat,
            'lng' => $this->lng
        ];
    }
}
