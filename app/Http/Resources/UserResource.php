<?php

namespace App\Http\Resources;

use App\Models\DelphosLicense;
use App\Models\UserProfile;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'created_at' => (string)$this->created_at,
            'delphos_license' => DelphosLicense::where('user_id', $this->id)->first()->hasLicense == 1 ? true : false,
            'default_media' => $this->media()->get() != null ? new MediaResource($this->media()->get()->last()) : null,
            'cover_media' => $this->mediaCover()->get() != null ? new MediaResource($this->mediaCover()->get()->last()) : null,
            'profile' => new UserProfileResource($this->profile),
            'country_options' => [
                'Deutschland',
                'Schweiz',
                'Österreich'
            ],
        ];
    }
}
