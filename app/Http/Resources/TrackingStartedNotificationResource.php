<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class TrackingStartedNotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource(User::find($this->user_id)),
            'tracking_id' => $this->tracking_id,
            'requested_user' => new UserResource(User::find($this->requested_user_id)),
            'active' => $this->active,
            'created_at' => (String)$this->created_at
        ];
    }
}
