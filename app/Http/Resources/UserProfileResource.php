<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'street' => $this->street,
            'zip' => $this->zip,
            'city' => $this->city,
            'phone' => $this->phone,
            'country' => $this->country,
            'age' => $this->age,
            'gender' => $this->gender,
            'height' => $this->height,
            'intolerances' => $this->intolerances,
            'pre_existing_conditions' => $this->pre_existing_conditions,
            'medication' => $this->medication,
            'blood_type' => $this->blood_type
        ];
    }
}
