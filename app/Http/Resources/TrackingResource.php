<?php

namespace App\Http\Resources;

use App\Models\Contacts;
use App\Models\ContactTracking;
use App\Models\Emergency;
use App\Models\Position;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class TrackingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource(User::find($this->user_id)),
            'delphos' => $this->delphos == 1 ? true : false,
            'emergency' => $this->emergency == 1 ? true : false,
            'emergency_details' => new EmergencyResource($this->emergency()->first()),
            'notify_period' => $this->notify_period,
            'position' => new PositionResource(Position::where('tracking_id', $this->id)->get()->last()),
            'start_pos' => new PositionResource(Position::where('tracking_id', $this->id)->first()),
            'isGuard' => Auth::guard('api')->user()->id == $this->user_id ? false : true,
            'status' => $this->status,
            'timer' => strtotime($this->update_countdown) - strtotime(Carbon::now()->format('Y-m-d H:i:s')) - 20,
            'cancelled' => $this->getCancelAttribute(),
            'created_at' => (string)$this->created_at,
        ];
    }

    private function getCancelAttribute(){

        $authUser = Auth::guard('api')->user();

        $contact = Contacts::where('sender_id', $this->user_id)->where('recipient_id', $authUser->id)->first();

        if ($contact){
            $contacttracking = ContactTracking::where('tracking_id', $this->id)->where('contact_id', $contact->id)->first();
            if ($contacttracking){
                $cancelled = $contacttracking->cancelled == 1 ? true : false;
            }else{
                $cancelled = false;
            }
        }else{
            $cancelled = false;
        }

        return $cancelled;
    }
}