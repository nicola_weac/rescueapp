<?php

namespace App\Http\Resources;

use App\Models\DefaultContacts;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'contact' => new UserResource(User::find($this->recipient_id)),
            'isDefault' => DefaultContacts::where('default_contact_id', $this->id)->where('user_id', $this->sender_id)->first() == null ? false : true
        ];
    }
}
