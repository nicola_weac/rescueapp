<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tracking_id');
            $table->double('from_lat');
            $table->double('from_lng');
            $table->string('from_city');
            $table->string('from_street');
            $table->string('from_zip');
            $table->string('to_lat');
            $table->string('to_lng');
            $table->string('to_city');
            $table->string('to_street');
            $table->string('to_zip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
