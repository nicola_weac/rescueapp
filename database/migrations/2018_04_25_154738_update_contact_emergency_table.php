<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_emergency', function (Blueprint $table){
            $table->dropColumn('lat');
            $table->dropColumn('lng');
            $table->dropColumn('city');
            $table->dropColumn('street');
            $table->dropColumn('zip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_emergency', function (Blueprint $table){
            $table->dropColumn('lat');
            $table->dropColumn('lng');
            $table->dropColumn('city');
            $table->dropColumn('street');
            $table->dropColumn('zip');
        });
    }
}
