<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profile', function (Blueprint $table){
            $table->integer('age')->nullable();
            $table->integer('height')->nullable()->comment('in cm');
            $table->text('intolerances')->nullable();
            $table->text('pre_existing_conditions')->nullable();
            $table->text('medication')->nullable();
            $table->string('blood_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profile', function (Blueprint $table){
            $table->dropColumn('age');
            $table->dropColumn('height');
            $table->dropColumn('intolerances');
            $table->dropColumn('pre_existing_conditions');
            $table->dropColumn('medication');
            $table->dropColumn('blood_type');
        });
    }
}
