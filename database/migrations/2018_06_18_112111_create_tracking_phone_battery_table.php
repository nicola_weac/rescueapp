<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingPhoneBatteryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_phone_battery', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tracking_id')->nullable();
            $table->integer('battery_lvl')->nullable();
            $table->string('battery_state')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_phone_battery');
    }
}
