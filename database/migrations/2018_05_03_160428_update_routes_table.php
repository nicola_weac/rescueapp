<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table){
            $table->integer('tracking_id')->unsigned()->change();
        });

        Schema::table('routes', function (Blueprint $table){
            $table->foreign('tracking_id')->references('id')->on('tracking')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes', function (Blueprint $table){
            $table->dropForeign('routes_tracking_id_foreign');
        });
    }
}
