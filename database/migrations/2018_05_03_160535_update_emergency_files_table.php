<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmergencyFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emergency_files', function (Blueprint $table){
            $table->integer('emergency_id')->unsigned()->change();
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('emergency_id')->references('id')->on('emergencies')->ondelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emergency_files', function (Blueprint $table){
            $table->dropForeign('emergency_files_emergency_id_foreign');
            $table->dropForeign('emergency_files_user_id_foreign');
        });
    }
}
