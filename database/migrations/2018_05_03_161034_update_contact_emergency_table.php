<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_emergency', function (Blueprint $table){
            $table->integer('emergency_id')->unsigned()->change();
            $table->integer('contact_id')->unsigned()->change();

            $table->foreign('emergency_id')->references('id')->on('emergencies')->ondelete('cascade');
            $table->foreign('contact_id')->references('id')->on('contact')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_emergency', function (Blueprint $table){
            $table->dropForeign('contact_emergency_emergency_id_foreign');
            $table->dropForeign('contact_emergency_contact_id_foreign');
        });
    }
}
