<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMembersProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members_profile', function (Blueprint $table){
            $table->integer('member_id')->unsigned()->change();
            $table->foreign('member_id')->references('id')->on('members')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members_profile', function (Blueprint $table){
            $table->dropForeign('members_profile_member_id_foreign');
        });
    }
}
