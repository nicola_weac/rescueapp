<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profile', function (Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('tracking', function (Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profile', function (Blueprint $table){
            $table->dropForeign('user_profile_user_id_foreign');
        });

        Schema::table('tracking', function (Blueprint $table){
            $table->dropForeign('tracking_user_id_foreign');
        });
    }
}
