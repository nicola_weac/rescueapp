<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_tracking', function (Blueprint $table){
            $table->integer('tracking_id')->unsigned()->change();
            $table->integer('contact_id')->unsigned()->change();

            $table->foreign('tracking_id')->references('id')->on('tracking')->ondelete('cascade');
            $table->foreign('contact_id')->references('id')->on('contact')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_tracking', function (Blueprint $table){
            $table->dropForeign('contact_tracking_tracking_id_foreign');
            $table->dropForeign('contact_tracking_contact_id_foreign');
        });
    }
}
