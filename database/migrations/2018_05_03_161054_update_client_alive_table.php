<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClientAliveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_alive', function (Blueprint $table){
            $table->integer('user_id')->unsigned()->change();
            $table->integer('tracking_id')->unsigned()->change();

            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
            $table->foreign('tracking_id')->references('id')->on('tracking')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_alive', function (Blueprint $table){
            $table->dropForeign('client_alive_user_id_foreign');
            $table->dropForeign('client_alive_tracking_id_foreign');
        });
    }
}
