<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_emergency', function (Blueprint $table){
            $table->renameColumn('tracking_id', 'emergency_id');
            $table->renameColumn('user_id', 'contact_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_emergency', function (Blueprint $table){
            $table->dropColumn('emergency_id');
            $table->dropColumn('contact_id');
        });
    }
}
