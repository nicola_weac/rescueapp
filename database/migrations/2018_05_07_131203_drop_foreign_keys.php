<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_tracking', function (Blueprint $table){
            $table->dropForeign('contact_tracking_contact_id_foreign');
        });

        Schema::table('contact_emergency', function (Blueprint $table){
            $table->dropForeign('contact_emergency_contact_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
