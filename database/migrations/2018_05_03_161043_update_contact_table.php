<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact', function (Blueprint $table){
            $table->integer('sender_id')->unsigned()->change();
            $table->integer('recipient_id')->unsigned()->change();

            $table->foreign('sender_id')->references('id')->on('users')->ondelete('cascade');
            $table->foreign('recipient_id')->references('id')->on('users')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact', function (Blueprint $table){
            $table->dropForeign('contact_sender_id_foreign');
            $table->dropForeign('contact_recipient_id_foreign');
        });
    }
}
