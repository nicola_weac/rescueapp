<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mediable_id');
            $table->string('mediable_type', 191);
            $table->string('filename', 191);
            $table->string('mime',191);
            $table->bigInteger('size');
            $table->string('name',191);
            $table->string('alt', 191);
            $table->string('title',191);
            $table->string('group',191);
            $table->boolean('status');
            $table->integer('weight');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
