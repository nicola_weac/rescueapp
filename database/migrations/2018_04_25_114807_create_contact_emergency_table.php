<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_emergency', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tracking_id');
            $table->integer('user_id');
            $table->double('lat');
            $table->double('lng');
            $table->string('city')->nullable();
            $table->string('street')->nullable();
            $table->string('zip', 5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_emergency');
    }
}
