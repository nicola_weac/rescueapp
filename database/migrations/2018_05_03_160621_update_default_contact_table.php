<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDefaultContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('default_contact', function (Blueprint $table){
            $table->integer('user_id')->unsigned()->change();
            $table->integer('default_contact_id')->unsigned()->change();

            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
            $table->foreign('default_contact_id')->references('id')->on('contact')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('default_contact', function (Blueprint $table){
            $table->dropForeign('default_contact_user_id_foreign');
            $table->dropForeign('default_contact_default_contact_id_foreign');
        });
    }
}
